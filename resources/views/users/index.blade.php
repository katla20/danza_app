@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"></h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
        </ol>
    </div>
    <!--<div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>-->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row" id="frameUsuarios">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body bg-inverse">
                    <h4 class="text-white card-title">Usuarios</h4>
                    <h6 class="card-subtitle text-white m-0 op-5">Crear, editar y eliminar usuarios</h6>
                </div>
                <div class="card-body">
                    <a class="btn btn-success" href="{{ route('users.create') }}">Nuevo usuario</a>
                    <div class="table-responsive">
                        <table id="usuarios" class="responsive table table-bordered table-striped table-hover">
                        <thead>
                                <tr>
                                    <!--<th>Estatus</th>-->
                                    <th data-priority="1">Nombre</th>
                                    <th>Email</th>
                                    <th data-priority="2">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td class="text-center">{{ $user->name }}</td>
                                    <td class="text-center">{{ $user->email }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ti-settings"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{ route('users.edit', $user->id) }}"><i class="fa fa-pencil"></i>  Editar</a>
                                                <div class="dropdown-divider"></div>
                                                <!-- <a class="dropdown-item" href="{{ route('users.destroy', $user->id) }}"><i class="fa fa-trash-o text-danger"></i></a> -->
                                                <button type="button" onclick="deleteUser('{{ $user->id }}')" class="dropdown-item"><i class="fa fa-trash-o text-danger"></i></button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</div>
    <!-- Column -->
@stop
@section('scripts')
<!--<script src="{{ asset('assets/modules_js/concept.script.js') }}"></script>-->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.js') }}"></script>
<!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->

<script>
    //const url = $("meta[name=base_url]").attr('content');
    //var res;
    function deleteUser(userId){
        const delUser = confirm('¿Desea eliminar el usuario?');

        if (delUser) {
            location.href = 'users/'+userId+'/destroy';
        }
    }

    $(document).ready(function() {
         $('#usuarios').DataTable( {
            "responsive": true,
            "lengthChange": true,
            "order": [ [ 0, "asc" ] ],
            /*"columnDefs": [
		            { responsivePriority: 1, targets: 2 },
		            { responsivePriority: 2, targets: 4 }
            ],*/
            "pageLength": 5,
            "lengthMenu" :[[5,10,25,50,100,-1],[5,10,25,50,100,"Todos"]],
            "language": {
                    "decimal":        "",
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primeo",
                        "last":       "Ultimo",
                        "next":       "Proximo",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": activar para ordenar la columna ascendente",
                        "sortDescending": ": activar para ordenar la columna descendente"
                    }
                }
        });

    });

</script>
@stop
@section('styles-top')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.dataTables.scss') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap.scss') }}">
@stop