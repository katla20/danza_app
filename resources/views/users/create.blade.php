@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"></h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Usuarios</a></li>
            <li class="breadcrumb-item active">Nuevo usuario</li>
        </ol>
    </div>
    <!--<div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>-->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row" id="frameUsuarios">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body bg-inverse">
                    <h4 class="text-white card-title">Nuevo usuario</h4>
                    <h6 class="card-subtitle text-white m-0 op-5">Ingrese el nuevo usuario</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('users.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type='text' name="name" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Usuario</label>
                            <input type='text' name="username" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type='text' name="email" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Academia</label>
                            <select name="academia_id" class="form-control" required>
                                <option value=""></option>
                                @foreach($academies as $academy)
                                    <option value="{{ $academy->academia_id }}">{{ $academy->academia_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type='password' name="password" class="form-control" required />
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
    <!-- Column -->
@stop