<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Recibo de pago</title>

        <!-- Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">-->

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <style>

            .table {
              margin-bottom: 0rem;
              border-spacing: 1px;
            }
            .table td, .table th {
             border-top: 0px solid #e9ecef;
             padding: .1rem;
            }
            .small, small {
              font-size: 50%;
              font-weight: 400;
            }
            .wrapper{
              border: 1px solid #1b1e21;
              margin-right: 10px;
            }
            .h6, h6 {
              font-size: 0.7rem;
              margin-bottom: .0rem;
            }
             .h5, h5 {
              font-size: 0.9rem;
              margin-bottom: .0rem;
            }
            .title{
            height: 2%;
            }
            .img-thumbnail {
               padding: 0rem;
               border: 0px solid #ddd;
               border-radius: 0rem;
            }
            .page-break {
                page-break-after: always;
            }

        </style>
    </head>
    <body>
      <div>
       <center><span>Datos de la Empresa</span></center>
      </div>
      <br>
      <h5>RECIBO DE PAGO</h5>
      <div class="col-lg-12 col-md-12">
        <li>
          <ul>ID Pago : 
            <span class="text-muted">
                # {{$balance->payment_id}}
            </span>
          </ul>
          <ul>Fecha Pago : 
            <span class="text-muted">
                <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balance->payment_dateinsert)->format('F d, Y')}}
            </span>
          </ul>
          <ul>ID Trabajador : #{{ $balance->employee->employee_id }}</ul>
          <ul>Datos del Trabajador : {{ $balance->employee->entity->entity_identity }} - {{ $balance->employee->entity->entity_name }} {{ $balance->employee->entity->entity_lastname }}</ul>
          <ul>Cargo: {{ $balance->employee->workplace->work_name }}</ul>
          <ul>Salario : {{ $balance->employee->employee_salary }} USD</ul>
          <ul>Fecha de Ingreso : 
            <span class="text-muted">
                <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balance->payment_dateinsert)->format('F d, Y')}}
            </span>
          </ul>
          <ul>
            <table class="table" border="1">
                <thead>
                  <tr>
                    <th>Codigo</th>
                    <th>Descripcion</th>
                    <th>Asignaciones</th>
                    <th>Deducciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>0001</td>
                    <td>Salario</td>
                    <td>{{ $balance->employee->employee_salary }}</td>
                    <td>0</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                     <td colspan="1"></td>
                     <td>Totales..</td>
                     <td>{{ $balance->employee->employee_salary }}</td>
                     <td>0</td>
                  </tr>
                  <tr>
                     <td colspan="2"></td>
                     <td>Neto a Cobrar</td>
                     <td>{{ $balance->employee->employee_salary }}</td>
                  </tr>
                </tfoot>
              </table>
          </ul>
          <ul>Billetera: {{$balance->employee->entity->wallets[0]->wallet_api }} {{$balance->employee->entity->wallets[0]->wallet_address }}</ul>
          <ul>Observacion Pago : {{ $balance->payment_observation }}</ul>
        </li>
     </div>
    </body>
</html>