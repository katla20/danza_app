@extends('layouts/admin/default')
{{-- Page title --}}
@section('title')
    Bienvenido a la plataforma 
    @parent
@stop
@section('page-titles')
<div class="row page-titles">
    <div class="col-md-4 item-desktop">
        <h4 class="text-white">RECIBO DE PAGO</h4>
    </div>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12">
      <div class="col-lg-12 col-md-12">
        <li>
          <ul>Fecha Pago : 
            <span class="text-muted">
                <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balance->payment_dateinsert)->format('F d, Y')}}
            </span>
          </ul>
          <ul>Monto Total : {{ $balance->payment_amount_usd }} USD</ul>
          <ul>Cantidad Total : {{ $balance->payment_amount_crypto }} BTC</ul>
          <ul>Observacion : {{ $balance->payment_observation }}</ul>
        </li>
     </div>
  </div>
</div>
<div class="form-actions">            
        <a class="btn btn-outline-inverse" href="{{ route('payments.index') }}">Volver</a>
        <a class="btn btn-warning" href="{{ route('payments.print_receipt',['id' => $balance->payment_id] )}}"><i class="fa fa-print"></i>Imprimir</a>
</div>
@stop