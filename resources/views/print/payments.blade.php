<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lista de programacion de pago descarga</title>

        <!-- Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">-->

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <style>

            .table {
              margin-bottom: 0rem;
              border-spacing: 1px;
            }
            .table td, .table th {
             border-top: 0px solid #e9ecef;
             padding: .1rem;
            }
            .small, small {
              font-size: 50%;
              font-weight: 400;
            }
            .wrapper{
              border: 1px solid #1b1e21;
              margin-right: 10px;
            }
            .h6, h6 {
              font-size: 0.7rem;
              margin-bottom: .0rem;
            }
             .h5, h5 {
              font-size: 0.9rem;
              margin-bottom: .0rem;
            }
            .title{
            height: 2%;
            }
            .img-thumbnail {
               padding: 0rem;
               border: 0px solid #ddd;
               border-radius: 0rem;
            }
            .page-break {
                page-break-after: always;
            }

        </style>
</head>
<body>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <li>
      <ul>Fecha Creación : 
        <span class="text-muted">
            <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balances->pay_dateinsert)->format('F d, Y')}}
        </span>
      </ul>
      <ul>Programado para el : 
        <span class="text-muted">
            <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balances->pay_datescheduling)->format('F d, Y')}}
        </span>
      </ul>

      <ul>Estatus del pago : <span class="label {{ $balances->label_estatus }}">{{ $balances->pay_estatus }}</span></ul>
      <ul>Monto Total : {{ $totales->total_usd }} USD</ul>
      <ul>Cantidad Total : {{ $totales->total_btc }} BTC</ul>
      <!--<ul>Express : {{ $balances->pay_express }}</ul>-->
      <ul>Observacion : {{ $balances->pay_observation }}</ul>
    </li>

  </div>
  <div class="col-lg-12 col-md-12">
  </br>
    <h5>LISTA DE PROGRAMACIÓN DE PAGO NRO #{{ $balances->pay_id }}</h5>
  </br>
    <table id="payments" class="table">
          <thead>
              <tr>
                  <th>ID</th>
                  <th>Empleado</th>
                  <th>Cantidad</th>
                  <th>Monto</th>
                  <th>Transacción</th>
              </tr>
          </thead>
          <tbody>
              @foreach($balances->payments as $balance)
                  <tr>
                      <td>{{ $balance->payment_id }}</td>
                      <td width="25%">
                          <div class="user-img">
                              <img src="{{ asset('assets/images/users/avatar_man.png') }}" width="40px" height="40px" alt="user" class="img-circle">
                                {{ $balance->employee->entity->entity_name }} {{ $balance->employee->entity->entity_lastname }} / {{ $balance->employee->entity->entity_identity }}
                          </div>
                      </td>
                      <td>
                        <div class="user-img">
                            <img src="{{ asset('assets/images/monedita_btc.png') }}" width="30px" height="30px" alt="btc" class="img-circle">
                             {{ $balance->payment_amount_crypto }}
                        </div>
                      </td>
                      <td>
                        {{ $balance->payment_amount_usd }} USD
                      </td>
                      <td>
                        <a href="">#{{ $balance->payment_id }}</a>
                      </td>
              @endforeach
            </tbody>
      </table>
  </div>
</div>
</body>