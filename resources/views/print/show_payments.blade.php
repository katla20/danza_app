@extends('layouts/admin/default')
{{-- Page title --}}
@section('title')
    Bienvenido a la plataforma 
    @parent
@stop
@section('page-titles')
<div class="row page-titles">
    <div class="col-md-4 item-desktop">
        <h4 class="text-white">DETALLES DE PROGRAMACION DE PAGO</h4>
    </div>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12">
    <li>
      <ul>Fecha Creación : 
        <span class="text-muted">
            <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balances->pay_dateinsert)->format('F d, Y')}}
        </span>
      </ul>
      <ul>Programado para el : 
        <span class="text-muted">
            <i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($balances->pay_datescheduling)->format('F d, Y')}}
        </span>
      </ul>

      <ul>Estatus del pago : <span class="label {{ $balances->label_estatus }}">{{ $balances->pay_estatus }}</span></ul>
      <ul>Monto Total : {{ $totales->total_usd }} USD</ul>
      <ul>Cantidad Total : {{ $totales->total_btc }} BTC</ul>
      <!--<ul>Express : {{ $balances->pay_express }}</ul>-->
      <ul>Observacion : {{ $balances->pay_observation }}</ul>
    </li>

  </div>
  <div class="col-lg-12 col-md-12">
  </br>
    <h5>LISTA DE PROGRAMACIÓN DE PAGO NRO #{{ $balances->pay_id }}</h5>
  </br>
    <table id="payments" class="table table-bordered table-striped">
          <thead>
              <tr>
                  <th>ID</th>
                  <th>Empleado</th>
                  <th>Cantidad</th>
                  <th>Monto</th>
                  <th>Transacción</th>
              </tr>
          </thead>
          <tbody>
              @foreach($balances->payments as $balance)
                  <tr>
                      <td>{{ $balance->payment_id }}</td>
                      <td width="25%">
                          <div class="user-img">
                              <img src="{{ asset('assets/images/users/avatar_man.png') }}" width="40px" height="40px" alt="user" class="img-circle">
                                {{ $balance->employee->entity->entity_name }} {{ $balance->employee->entity->entity_lastname }} / {{ $balance->employee->entity->entity_identity }}
                          </div>
                      </td>
                      <td>
                        <div class="user-img">
                            <img src="{{ asset('assets/images/monedita_btc.png') }}" width="30px" height="30px" alt="btc" class="img-circle">
                             {{ $balance->payment_amount_crypto }}
                        </div>
                      </td>
                      <td>
                        {{ $balance->payment_amount_usd }} USD
                      </td>
                      <td>
                        <a href="">#{{ $balance->payment_id }}</a>
                      </td>
              @endforeach
            </tbody>
      </table>
      <div class="form-actions">            
        <a class="btn btn-outline-inverse" href="{{ route('payments.transactions') }}">Volver</a>
        <a class="btn btn-warning" href="{{ route('payments.print',['id' => $balances->pay_id] )}}"><i class="fa fa-print"></i>Imprimir</a>
      </div>
  </div>
</div>
@stop