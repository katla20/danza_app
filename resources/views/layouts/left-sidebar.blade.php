<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
                @if(Auth::user()->hasRole('admin'))
                    <ul id="sidebarnav">
                        <li>
                            <a class=" waves-effect waves-dark" href="{{ route('presentaciones') }}" aria-expanded="false"><i class="fa fa-child"></i><span class="hide-menu">Presentaciones</span></a>
                        </li>
                        <!--<li>
                            <a class="waves-effect waves-dark" href="{{ route('calificaciones') }}" aria-expanded="false"><i class="fa fa-trophy"></i><span class="hide-menu">Pura Danza</span></a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="{{ route('grandprix') }}" aria-expanded="false"><i class="fa fa-trophy"></i><span class="hide-menu">Gran Prix</span></a>
                        </li>-->
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">Resultados</span></a>
                            <ul class="collapse" aria-expanded="false">
                                <li>
                                    <a class="waves-effect waves-dark" href="{{ route('resultados') }}"><i class="fa fa-list"></i><span> Resultados Pura Danza</span></a>
                                </li>
                                <li>
                                    <a class="waves-effect waves-dark" href="{{ route('resultados.grandprix') }}"><i class="fa fa-list"></i><span> Resultados Grand Prix</span></a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">Registros</span></a>
                            <ul class="collapse" aria-expanded="false">
                                <li>
                                    <a href="{{ route('users.index') }}">Usuarios</a>
                                </li>
                                <li>
                                    <a href="{{ route('academies.index') }}">Academias</a>
                                </li>
                            </ul>
                        </li>
                        
                    </ul>
                @else
                    <ul id="sidebarnav"> 
                        <li>
                            <a class="nav-link" href="{{ route('calificaciones') }}" aria-expanded="false"><i class="fa fa-home"></i></a>
                        </li>
                        <li class="pl-5" > 
                            <a class="has-arrow waves-effect waves-dark" href="{{ route('calificaciones') }}" aria-expanded="false"><i class="fa fa-trophy"></i><span class="hide-menu">Pura Danza</span></a>
                        </li>
                        <li> 
                            <a class="has-arrow waves-effect waves-dark" href="{{ route('grandprix') }}" aria-expanded="false"><i class="fa fa-trophy"></i><span class="hide-menu">Gran Prix</span></a>
                        </li>
                    </ul>
                @endif
        </nav>
        
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>