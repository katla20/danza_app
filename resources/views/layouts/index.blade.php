@extends('layouts/default')

@section('content')
<div class="row">
    <!-- Column -->
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round align-self-center round-warning"><i class="fa fa-exclamation-triangle"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h3 class="m-b-0">{{$widgets['Pendiente']}} Presentaciones</h3>
                        <h5 class="text-muted m-b-0">Pendientes</h5></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round align-self-center round-success"><i class="fa fa-check"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h3 class="m-b-0">{{$widgets['Ejecutada']}} Presentaciones</h3>
                        <h5 class="text-muted m-b-0">Ejecutadas</h5>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round align-self-center round-success"><i class="fa fa-check"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h3 class="m-b-0">{{$widgets['All']}} Presentaciones</h3>
                        <h5 class="text-muted m-b-0">Todas</h5>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<div class="row">
    <!--<div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Participantes</h5>
                <div class="table-responsive">
                    <table id="participantes-dash" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Edad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datos as $dato)
                                <tr>
                                    <td>{{$dato->participantes[0]->participante_name}} </td>
                                    <td>{{\Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($dato->participantes[0]->participante_fechanac))}} años </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
    </div>-->
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
            @if(Auth::user()->hasRole('admin'))
            <a class="btn btn-success" href="{{ route('presentaciones') }}">Ir..</a>
            @endif
                <div class="table-responsive m-t-40">
                    <table id="presentaciones" class="responsive table table-striped ">
                    <thead>
                            <tr>
                                <th width="10%"># Present.</th>
                                <th width="15%">Concurso</th>
                                <th width="15%">Fecha Ejec.</th>
                                <th>Participante/Grupo</th>
                                <!--<th>Variacion</th>-->
                                <th>Categoria</th>
                                <th>Division</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.js') }}"></script>
<script>
$(document).ready(function() {

$('#presentaciones').DataTable( {
    "order": [ [ 2, "desc" ] ],
    "pageLength": 5,
    "lengthMenu" :[[5,10,25,50,100,-1],[5,10,25,50,100,"Todos"]],
    "language": {
            "decimal":        "",
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(filtrado de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron registros coincidentes",
            "paginate": {
                "first":      "Primeo",
                "last":       "Ultimo",
                "next":       "Proximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activar para ordenar la columna ascendente",
                "sortDescending": ": activar para ordenar la columna descendente"
            }
        },
        processing: true,
        serverSide: true,
        ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{!! route('presentaciones.datatable') !!}',
                type: 'POST',
                data: function (d) {}
        },
        'columns': [
            {data: 'id', name: 'id'},
            {data: 'concurso', name: 'concurso'},
            {data: 'fecha', name: 'fecha'},
            {data: 'participante', name: 'participante'},
            //{data: 'variacion', name: 'variacion'},
            {data: 'categoria', name: 'categoria'},
            {data: 'division', name: 'division'},
            {data: 'status', name: 'status'},         
        ]
    });

});

</script>
@stop
@section('styles-top')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.dataTables.scss') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap.scss') }}">
@stop
