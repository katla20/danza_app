@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!--<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"></h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Resultados Grand Prix</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>-->
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row" id="resultados-data">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <section class="search-sec">                  
                    <div class="container">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        <form action="#" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-3 col-sm-12 p-2">
                                            <div class="input-group"> 
                                                <select class="form-control custom-select" id="fecha">
                                                    <option selected value="">fechas</option>
                                                    @foreach ($flt_fechas as $fecha)
                                                        <option value="{{$fecha->pres_fecha}}">{{$fecha->pres_fecha}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-3 col-sm-12 p-2">
                                            <select class="form-control custom-select" id="division">
                                                <option selected value="">division</option>
                                                @foreach ($flt_divisiones as $division)
                                                    <option value="{{$division->division_name}}">{{$division->division_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-3 col-sm-12 p-2">
                                            <select class="form-control custom-select" id="tecnica">
                                                <option selected value="">tecnica</option>
                                                @foreach ($flt_tecnicas as $tecnica)
                                                    <option value="{{$tecnica->tecnica_name}}">{{$tecnica->tecnica_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-11 align-self-start'><button type="button" id="filters" class="btn btn-info waves-effect waves-light">Filtrar</button></div>
                                        <div class='col-1 align-self-end'><button type="reset" id="clear" class="btn waves-effect waves-light"><i class="mdi mdi-broom"></i></button></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div> 
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body bg-inverse">
                    <h4 class="text-white card-title">Resultados Grand Prix</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="resultados" class="responsive table table-bordered table-striped table-hover">
                        <thead>
                                <tr>
                                    <th>Present.</th>
                                    <th>Participante</th>
                                    <th>Variacion</th>
                                    <th>Division</th>
                                    <th>Puntaje</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</div>
@stop
@section('scripts')
<!--<script src="{{ asset('assets/modules_js/concept.script.js') }}"></script>-->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>

    $(document).ready(function() {

        $('#resultados').DataTable( {
            "ordering": true,
            "responsive": true,
            "lengthChange": true,
            "order": [ [ 1, "asc" ] ],
            //"iDisplayLength": -1,
            "pageLength": 5,
            "lengthMenu" :[[5,10,25,50,100,-1],[5,10,25,50,100,"Todos"]],
            "language": {
                    "decimal":        "",
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "<i class='fa fa-search'></i> _INPUT_",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primeo",
                        "last":       "Ultimo",
                        "next":       "Proximo",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": activar para ordenar la columna ascendente",
                        "sortDescending": ": activar para ordenar la columna descendente"
                    }
            },
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{!! route('resultados.datatable',['concurso' => 2]) !!}',
                type: 'POST',
                data: function (d) {
                    if($("#division option:selected").val()!=""){
                        d.division=$("#division option:selected").val();
                    }
                    if($("#fecha option:selected").val()!=""){
                        d.fecha=$("#fecha option:selected").val();
                    }
                    if($("#tecnica option:selected").val()!=""){
                        d.tecnica=$("#tecnica option:selected").val().substr(6);
                    }
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'participante', name: 'participante'},
                {data: 'variacion', name: 'variacion'},
                {data: 'division', name: 'division'},
                {data: 'puntaje', name: 'puntaje'},
                {data: 'total', name: 'total'},
                
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
                dom: "Blfrtip",
                buttons: [
                    {
                    text: '<i class="fa fa-print"></i>',
                    extend: 'print',
                    titleAttr: 'imprimir',
                    customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '9pt' );
                            $(win.document.body).find( 'table' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' );
                        }
                    },
                    {
                    text: '<i class="fa fa-file-excel-o"></i>',
                    extend: 'excel',
                    titleAttr: 'excel'
                    },
                    {
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    extend: 'pdf',
                    titleAttr: 'pdf'
                    },
                    {
                    text: '<i class="fa fa-refresh"></i>',
                        action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        }
                    }
                ],  
            });

            //boton de filtros automaticos
        $('#filters').on('click', function(e) {
            $('#resultados').DataTable().draw(true);
            e.preventDefault();
        });

    });
    
</script>
@stop
@section('styles-top')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.dataTables.scss') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap.scss') }}">
<style type="txt/css">
.search-sec{
    background: #1A4668;padding: 2rem;
}
.search-slt{
    display: block;
    width: 100%;
    font-size: 0.875rem;
    line-height: 1.5;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.wrn-btn{
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    text-transform: capitalize;
     height: calc(3rem + 2px) !important;
     border-radius:0;
}

#button-group {
  margin: auto;
  display: flex;
  flex-direction: row;
  justify-content: center;
}
</style>
@stop


