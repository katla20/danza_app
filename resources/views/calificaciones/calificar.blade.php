<div class="row">
    <!-- Column -->
    <div class="col-lg-5 col-md-12">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30">
                    <img src="{{ asset('assets/images/votaciones/disco-ball.png') }}" class="img-circle" width="100">
                    <h4 class="card-title m-t-10">{{ $datos->variacion->variacion_name }}</h4>
                    <h6 class="card-subtitle">{{ $datos->tecnica->tecnica_name }}</h6>
                    <div class="row text-center justify-content-md-center">
                        <!--<div class="col-6"><i class="fa fa-clock-o"></i><font class="font-medium">{{$datos->pres_tiempo}}</font></div>-->
                        <div class="col-12"><i class="fa fa-calendar"></i><font class="font-medium">{{$datos->pres_fecha}}</font></div>
                    </div>
                    @if($datos->pres_link)
                    <div class="row">
                        <div class="col-8 col-md-12 mt-2">
                            <a href="{{ $datos->pres_link }}" style ="font-size: 14px" target="_blank">{{ $datos->pres_link }}</a>
                            <!--<iframe width="300" height="200" src="https://www.youtube.com/embed/85MppyLJHz0"  allowfullscreen></iframe>-->
                        </div>
                    </div>
                    @endif
                </center>
            </div>
            <div><hr></div>
            <div class="card-body">
            <h6 class="card-subtitle">Participantes</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th width="20%">Edad</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($datos->participantes as $participante)
                            <tr>
                                <td>{{$participante->participante_name}}</td>
                                <td>{{\Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($participante->participante_fechanac))}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-7 col-md-12">
        <div class="card">
            <div class="card-body bg-inverse">
                    <h5 class="text-white card-title">Calificar la presentacion / rango puntaje 1-10</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <form id="frmcalificar" method="post">
                    {{csrf_field()}}
                        <table id="presentaciones-dash" class="table">
                            <tbody>
                            
                                @foreach($items as $item)
                                    <tr>
                                        <td width="5%">#</td>
                                        <td>{{ $item->consideracion_name }}</td>
                                        <td width="30%">
                                            <input type="number" id="puntaje" class="form-control decimal-number" onkeypress="return NumCheck(event, this)" onChange="return limites(event, this)" name="puntaje:{{ $item->consideracion_id}}" maxlenght="2" value="" size="3">
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">
                                        <input type="hidden" name="id" value="{{$datos->pres_id}}">
                                        <textarea class="form-control" rows="8" name="observaciones" placeholder="Obsevaciones"></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
