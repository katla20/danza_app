@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Detalle Presentacion</li>
        </ol>
    </div>
    <!--<div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>-->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-5 col-md-12">
        <div class="card">
            <div class="card-body">
                <a href="{{ route('resultados.grandprix') }}"><i class="mdi mdi-arrow-left"></i>volver</a>
                <center class="m-t-30">
                    <img src="{{ asset('assets/images/votaciones/disco-ball.png') }}" class="img-circle" width="100">
                    <h4 class="card-title m-t-10">{{ $datos->variacion->variacion_name }}</h4>
                    <h6 class="card-subtitle">{{ $datos->categoria->categoria_name }}</h6>
                    <h6 class="card-subtitle">{{ $datos->tecnica->tecnica_name }}</h6>
                    <div class="row text-center justify-content-md-center">
                    <div class="col-6"><i class="fa fa-clock-o"></i><font class="font-medium">{{$datos->pres_tiempo}}</font></div>
                        <div class="col-6"><i class="fa fa-calendar"></i><font class="font-medium">{{$datos->pres_fecha}}</font></div>
                    </div>
                    @if($datos->pres_link)
                    <div class="row">
                        <div class="col-8 col-md-12 mt-2">
                            <a href="{{ $datos->pres_link }}" style ="font-size: 14px" target="_blank">{{ $datos->pres_link }}</a>
                            <!--<iframe width="300" height="200" src="https://www.youtube.com/embed/85MppyLJHz0"  allowfullscreen></iframe>-->
                        </div>
                    </div>
                    @endif
                    <div><hr></div>
                    <h1 class="text-center justify-content-md-center">{{$datos->total}}</h1>
                </center>
            </div>
            <div><hr></div>
            <div class="card-body">
            <h6 class="card-subtitle">Participantes</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th width="20%">Edad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datos->participantes as $participante)
                                <tr>
                                    <td>{{$participante->participante_name}}</td>
                                    <td>{{\Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($participante->participante_fechanac))}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-7 col-md-12">
    @foreach($datos->calificaciones[0] as $calificacion)
        <div class="card">
            <div class="card-body bg-inverse">
                    <h5 class="text-white card-title">{{$calificacion->jurado->jurado_name}}</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                        <table id="presentaciones-dash" class="table">
                            <tbody>
                                    @foreach($calificacion->puntajes as $puntaje)
                                        <tr>
                                            <td width="5%">#{{$puntaje->consideracion->consideracion_id}}</td>
                                            <td>{{$puntaje->consideracion->consideracion_name}}</td>
                                            <td width="30%">
                                            {{$puntaje->puntaje}}
                                            </td>
                                        </tr>
                                    @endforeach
                                        <tr class="font-weight-bold">
                                           <td colspan="2">
                                            Total:
                                            </td>
                                            <td>
                                            {{$calificacion->total}}
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td colspan="3">
                                            {{$calificacion->calif_observacion}}
                                            </td>
                                        </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>

@stop
<script>
</script>