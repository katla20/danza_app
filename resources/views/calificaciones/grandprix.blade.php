@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!--<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"></h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Grand Prix</li>
        </ol>
    </div>
    <!--<div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>-->
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row" id="">
<div class="col-lg-3 col-md-12">
    <form action="#" method="post" novalidate="novalidate">
        <ul class="list-group">
            <li class="list-group-item bg-inverse text-white">
                <div class="row">
                    <div class='col-2 align-self-start'><i class="fa fa-filter" aria-hidden="true"></i></div>
                    <div class='col-10 align-self-end'><h4 class="text-white">Filtrar para Calificar</h4></div>
                </div>
            <li class="list-group-item">
                <div class='input-group date'>
                    <select class="form-control custom-select" id="fecha">
                        <option selected value="">...</option>
                        @foreach ($flt_fechas as $fecha)
                            <option value="{{$fecha->pres_fecha}}">{{$fecha->pres_fecha}}</option>
                        @endforeach
                    </select>
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </li>
            <li class="list-group-item">       
                <div class="input-group"> 
                    <span class="input-group-addon">Turno</span>
                    <select class="form-control custom-select" id="turno">
                        <option selected value="">...</option>
                        @foreach ($flt_turnos as $turno)
                            <option value="{{$turno->pres_turno}}">{{$turno->pres_turno}}</option>
                        @endforeach
                    </select>
                </div>
            </li>
            <li class="list-group-item">       
                <div class="input-group"> 
                    <span class="input-group-addon">Lugar</span>
                    <select class="form-control custom-select" id="lugar">
                        <option selected value="">...</option>
                        @foreach ($flt_lugar as $lugar)
                            <option value="{{$lugar->pres_lugar}}">{{$lugar->pres_lugar}}</option>
                        @endforeach
                    </select>
                </div>
            </li>
            <li class="list-group-item">
                <div class="input-group"> 
                    <span class="input-group-addon">Categoria</span>
                    <select class="form-control custom-select" id="categoria">
                        <option selected value="">...</option>
                        @foreach ($flt_categorias as $categoria)
                            <option value="{{$categoria->categoria_name}}">{{$categoria->categoria_name}}</option>
                        @endforeach
                    </select>
                </div>
            </li>
            <li class="list-group-item">                     
                <div class="input-group"> 
                    <span class="input-group-addon">Division</span>
                    <select class="form-control custom-select" id="division">
                        <option selected value="">...</option>
                        @foreach ($flt_divisiones as $division)
                            <option value="{{$division->division_name}}">{{$division->division_name}}</option>
                        @endforeach
                    </select>
                </div>
            </li>
            <li class="list-group-item">       
                <div class="input-group"> 
                    <span class="input-group-addon">Tecnica</span>
                    <select class="form-control custom-select" id="tecnica">
                        <option selected value="">...</option>
                        @foreach ($flt_tecnicas as $tecnica)
                            <option value="{{$tecnica->tecnica_name}}">{{$tecnica->tecnica_name}}</option>
                        @endforeach
                    </select>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class='col-9 align-self-start'><button type="button" id="filters" class="btn btn-info waves-effect waves-light">Filtrar</button></div>
                    <div class='col-3 align-self-end'><button type="reset" id="clear" class="btn waves-effect waves-light"><i class="mdi mdi-broom"></i></button></div>
                </div> 
            </li>
        </ul>  
    </form> 
    </div>
    <div class="col-lg-9 col-md-12">
        <div class="card">
            <div class="card-body bg-inverse">
                    <h4 class="text-white card-title">Grand Prix</h4>
                    <!--<h6 class="card-subtitle text-white m-0 op-5">Seleccione una presentacion que desea calificar y oprima el boton play</h6>-->
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="calificaciones" class="responsive table table-bordered table-striped table-hover">
                        <thead>
                                <tr>
                                    <th width="10%"></th>
                                    <th>Present.</th>
                                    <th>Participante</th>
                                    <!--<th>Tecnica</th>-->
                                    <th>Variacion/Tiempo</th>
                                    <!--<th>Turno/Lugar</th>-->
                                    <th>Division</th>
                                    <!--<th>Fecha Ejec.</th>-->
                                    
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</div>
<div id="calificar" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="calificarModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-youtube 2x"></i> Calificar Grand Prix</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btnguardar" onClick="calificar()"  class="btn btn-danger waves-effect waves-light">Calificar</button>
            </div>
        </div>
    </div>
</div>
    <!-- Column -->
@stop
@section('scripts')
<!--<script src="{{ asset('assets/modules_js/concept.script.js') }}"></script>-->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.js') }}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>

function openModal(id){
    const div ="#calificar .modal-body";

    axios.get("calificar/"+id)
        .then((res) => {
            $('#calificar').modal();
            $('#calificar').on('shown.bs.modal', function(){
                $(div).html(res.data);
            });
            $('#calificar').on('hidden.bs.modal', function(){
                $(div).data('');
            });

        }).catch(err => {
            console.log(err);
            alert(err);
        });
 
}

function calificar(){
        //config: { headers: {'Content-Type': 'multipart/form-data' }}}
       // const formData = new FormData();
    var data = $('#frmcalificar').serializeArray().reduce(function(obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});


    // console.log('get element',JSON.stringify(data));

    axios.post('calificar/guardar',data)
        .then((res) => {
            if(res.data==""){
                $('#calificaciones').DataTable().ajax.reload();
                $('#calificar').modal('hide'); 
            }else{
            console.log('func-->',JSON.stringify(res.data));
            }
            
        }).catch(err => {
            console.log(err);
            alert(err);
        });

}

function limites(e, field){
    if(field.value > 10.99){
        alert('fuera de rango 1-10');
        document.getElementById("puntaje").value = "0";
    }
}

function NumCheck(e, field) {

    key = e.keyCode ? e.keyCode : e.which
    // backspace
    if (key == 8) return true
    // 0-9
    if (key > 47 && key < 58) {
        if (field.value === "") return true;
            var existePto = (/[.]/).test(field.value);
        if (existePto === false){
            regexp = /.[0-9]{1}$/; //PARTE ENTERA 2
        }
        else {
            regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
        }
        return !(regexp.test(field.value));
    }
    // .
    if (key == 46) {
        if (field.value == "") return false
        regexp = /^[0-9]+$/
        return regexp.test(field.value)
    }
    // other key
    return false
}

    $(document).ready(function() {

        $('#calificaciones').DataTable( {
        "ordering": true,
        "responsive": true,
        "lengthChange": true,
        "order": [ [ 2, "asc" ] ],
        "pageLength": 5,
        "lengthMenu" :[[5,10,25,50,100,-1],[5,10,25,50,100,"Todos"]],
        "language": {
                "decimal":        "",
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                "infoFiltered":   "(filtrado de _MAX_ total registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "<i class='fa fa-search'></i> _INPUT_",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                    "first":      "Primeo",
                    "last":       "Ultimo",
                    "next":       "Proximo",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activar para ordenar la columna ascendente",
                    "sortDescending": ": activar para ordenar la columna descendente"
                }
            },
            processing: true,
            serverSide: true,
            searching: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{!! route('calificaciones.datatable',['concurso' => 2]) !!}',
                type: 'POST',
                data: function (d) {
                    
                    if($("#tecnica option:selected").val()!=""){
                        d.tecnica=$("#tecnica option:selected").val().substr(6);
                    }
                    if($("#categoria option:selected").val()!=""){
                        d.categoria=$("#categoria option:selected").val();
                    }
                    if($("#division option:selected").val()!=""){
                        d.division=$("#division option:selected").val();
                    }
                    if($("#turno option:selected").val()!=""){
                        d.turno=$("#turno option:selected").val();
                    }
                    if($("#lugar option:selected").val()!=""){
                        d.lugar=$("#lugar option:selected").val();
                    }
                    if($("#fecha option:selected").val()!=""){
                        d.fecha=$("#fecha option:selected").val();
                    }
                }
            },
            columns: [
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'id', name: 'id'},
                {data: 'participante', name: 'participante'},
               // {data: 'tecnica', name: 'tecnica'},
                {data: 'variacion', name: 'variacion'},
               // {data: 'turno', name: 'turno'},*/
                {data: 'division', name: 'division'},
                //{data: 'fecha', name: 'fecha Ejec.'},
            ]
            });

            var table = $('#calificaciones').DataTable();

            $('.dataTables_filter input').unbind().keyup(function(e) {
            //console.log(this.value.length);
                if(this.value.length==0){
                    console.log('menor a 0');
                    table.columns(1).search('').draw();
                    e.preventDefault();
                }else{
                    table.columns(1).search( this.value ).draw();
                }
            });

            //boton de filtros automaticos
            $('#filters').on('click', function(e) {
                $('#calificaciones').DataTable().draw(true);
                e.preventDefault();
            });


    });
    
</script>
@stop
@section('styles-top')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.dataTables.scss') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap.scss') }}">
@stop


