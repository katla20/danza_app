@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Presentaciones</li>
        </ol>
    </div>
    <!--<div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>-->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row" id="framePresentacion">
    <div class="col-lg-12 col-md-12">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Seleccione el excel de presentaciones grand prix
        </div>
        <div class="card-body">
            <form action="{{ route('importar-grandprix') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="file" name="file" class="form-control">
                </br>
                <button class="btn btn-success">Guardar</button>
                </br>
            </form>
        </div>
    </div>
    </div>
</div>
@stop
<script>
</script>