@extends('layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"></h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('academies.index') }}">Academias</a></li>
            <li class="breadcrumb-item active">Editar academia</li>
        </ol>
    </div>
    <!--<div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>-->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row" id="frameUsuarios">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body bg-inverse">
                    <h4 class="text-white card-title">Editar academia</h4>
                    <h6 class="card-subtitle text-white m-0 op-5">Editar datos de la academia</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('academies.update', $academy->academia_id) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type='text' name="academia_name" value="{{ $academy->academia_name }}" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Correo electrónico</label>
                            <input type='text' name="academia_correo" value="{{ $academy->academia_correo }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type='text' name="academia_telefono" value="{{ $academy->academia_telefono }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Directora</label>
                            <input type='text' name="academia_directora" value="{{ $academy->academia_directora }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>División</label>
                            <input type='text' name="academia_division" value="{{ $academy->academia_division }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>País</label>
                            <input type='text' name="pais" value="{{ $academy->pais }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <input type='text' name="estado" value="{{ $academy->estado }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type='text' name="ciudad" value="{{ $academy->ciudad }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Estatus</label>
                            <select name="status" class="form-control" />
                                <option selected disabled></option>
                                <option value="TRUE">Activa</option>
                                <option value="FALSE">Eliminada</option>
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">Guardar cambios</button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
    <!-- Column -->
@stop