/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
/*http://www.sacheprofessional.com.br/js/sweetalert2-master/example/*/

require("./bootstrap");

window.Vue = require("vue");


window.$ = window.jQuery = require("jquery");
window.VeeValidate = require("vee-validate");
window.BootstrapVue = require("bootstrap-vue");

import VueRouter from "vue-router";

import router from "./routes";

import swal from "sweetalert2";

import axios from "axios";

require("./plugins/sweet-alert-plugin");

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(axios);

//Vue.use(VeeValidate);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const url = $("meta[name=base_url]").attr("content");

const app= new Vue({
  el: "#main-wrapper",
  router,
  component: {},
  created: function() {
    console.log("Iniciando apps");
  },
  mounted: function() {},
  computed: {},
  data: {
    errors: {},
  },
  methods: {
  }
});
