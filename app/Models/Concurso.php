<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Concurso extends Model
{
    protected $table = 'concurso';
    protected $primaryKey = 'concurso_id';

    public function items()//consideraciones
    {
        return $this->belongsToMany('App\Models\Consideracion', 'concurso_consideraciones', 'concurso_id', 'consideracion_id');
    }

    public function scopeConcursoPresentaciones($query) {

        return  $query->with('participantes.academia')
                     ->with('presentaciones.categoria')
                     ->with('presentaciones.variacion')
                     ->with('presentaciones.division');
    }
    
}
