<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Academia extends Model
{
    protected $table = 'academia';
    protected $primaryKey = 'academia_id';

    protected $fillable = [
        'academia_name',
        'academia_correo',
        'academia_telefono',
        'academia_directora',
        'academia_division',
        'pais',
        'estado',
        'ciudad',
        'estatus',
    ];
}
