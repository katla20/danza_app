<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Academia;

class Participante extends Model
{
    protected $table = 'participante';
    protected $primaryKey = 'participante_id';
    protected $fillable = array(
        'participante_name',
        'participante_identificacion',
        'academia_id',
        'participante_fechanac',
        'participante_direccion',
        'pres_id',

    );
    public $timestamps = true;

    public function academia(){
	    return $this->belongsTo('App\Models\Academia','academia_id');
    }

    public function presentacion(){
	    return $this->belongsTo('App\Models\Presentacion','pres_id');
    }

    public function scopeFiltroAcademias($query) {

        return $query->select('academia.*')
                ->join('academia', function($join){
                    $join->on('academia.academia_id', '=', 'participante.academia_id');                       
                })
                ->groupBy('academia.academia_id')
                ->orderBy('academia.academia_id');
    }

}
