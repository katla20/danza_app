<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalificacionPuntaje extends Model
{
    protected $table = 'calificacion_puntaje';
    protected $primaryKey = 'califp_id';
    protected $fillable = array(
        'calif_id',
        'consideracion_id',
        'puntaje'
    );
    public $timestamps = true;

    public function consideracion(){
        return $this->belongsTo('App\Models\Consideracion','consideracion_id','consideracion_id');
    }

    public function calificacion(){
        return $this->belongsTo('App\Models\Calificacion','calif_id','calif_id');
    }
}
