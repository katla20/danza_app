<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = 'calificacion';
    protected $primaryKey = 'calif_id';
    protected $fillable = array(
        'calif_fecha',
        'pres_id',
        'jurado_id',
        'calif_observacion'
    );

    public $timestamps = true;

    public function jurado(){
        return $this->belongsTo('App\Models\Jurado','jurado_id');
    }

    public function puntajes(){
        return $this->hasMany('App\Models\CalificacionPuntaje','calif_id');
    }

    public function presentacion()
    {
        return $this->belongsTo('App\Models\Presentacion','pres_id');//->where('status','Ejecutada');
    }






}
