<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PresentacionParticipantes extends Model
{
    protected $table = 'presentacion_participantes';
    protected $primaryKey = 'presentacion_participantes_id';

    protected $fillable = array(
        'pres_id',
        'participante_id',
    );
    public $timestamps = true;


    public function presentaciones(){
	    return $this->belongsTo('App\Models\Presentacion','pres_id');
    }

    public function participantes (){
        return $this->belongsTo('App\Models\Participante','participante_id');
    }

}
