<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;//Auth::user()->id;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;
use DB;

use App\Models\Calificacion;


class Presentacion extends Model
{
    protected $table = 'presentacion';
    protected $primaryKey = 'pres_id';

    protected $dateFormat = 'd-m-Y';

    protected $casts = [
        'pres_fecha'  => 'date:d-m-Y'
    ];

    protected $fillable = array(
        'pres_fecha',
        'variacion_id',
        'division_id',
        'categoria_id',
        'concurso_id',
        'tecnica_id',
        'pres_turno',
        'pres_lugar',
        'pres_tiempo',
        'pres_link',
        'pres_grupo',
        'pres_observacion',
        'status'
    );

    public $timestamps = true;


    public function concurso(){
        return $this->belongsTo('App\Models\Concurso','concurso_id');
    }

    public function division(){
	    return $this->belongsTo('App\Models\Division','division_id');
    }

    public function variacion(){
	    return $this->belongsTo('App\Models\Variacion','variacion_id');
    }

    public function categoria(){
	    return $this->belongsTo('App\Models\Categoria','categoria_id');
    }

    public function tecnica(){
	    return $this->belongsTo('App\Models\Tecnica','tecnica_id');
    }
    
    public function participantes(){
        return $this->hasMany('App\Models\Participante','pres_id');
    }

    public function calificaciones(){
        return $this->hasMany('App\Models\Calificacion','pres_id');
    }


    public function scopeDashboard($query) {

        $data=$query->select(DB::raw('count(*) as totales, status'))
                     ->groupBy('status')
                     ->get();
        
        return $data;
      
    }

    public function scopeConcursoPresentaciones($query,$concurso) {

        return  $query->with('presentaciones.concurso'); 
    }

    public function scopeFiltroFechas($query) {

        return $query
                //->select(DB::raw('CONCAT(roles.name, " ", teams.name) AS function'))
                ->select('pres_fecha')
                ->groupBy('pres_fecha')
                ->orderBy('pres_fecha');
    }


    public function scopeFiltroTurnos($query) {

        return $query
                ->select('pres_turno')
                ->groupBy('pres_turno')
                ->orderBy('pres_turno');
    }

    public function scopeFiltroLugares($query) {

        return $query
                ->select('pres_lugar')
                ->groupBy('pres_lugar')
                ->orderBy('pres_lugar');
    }

    public function scopeGetById($query,$id) {

        return  $query->with('categoria')
                        ->with('variacion')
                        ->with('division')
                        ->with('concurso')
                        ->with('tecnica')
                        ->with('participantes.academia')
                        ->where('pres_id',$id);
    }

    public function scopeList($query) {

        return  $query->with('categoria')
                    ->with('variacion')
                    ->with('division')
                    ->with('tecnica')
                    ->with('concurso.items')
                    ->with('participantes.academia');
    }

    public function scopeListResult($query,$pres_id) {
        //resultado de las calificaciones

        return  $query->with('categoria')
                    ->with('variacion')
                    ->with('division')
                    ->with('tecnica')
                    ->with('participantes.academia')
                    ->with('calificacion.puntajes.consideracion')
                    ->with('calificacion.jurado')
                    ->where('status','Ejecutada')
                    ->where('pres_id',$pres_id);
    }

    public function scopeDataCalificaciones($query,$concurso,$jurado) {

            $data=$query->select(DB::raw("presentacion.*"))
                    ->with('categoria')
                    ->with('variacion')
                    ->with('division')
                    ->with('tecnica')
                    ->with('participantes')
                    ->with('calificaciones')
                    ->leftJoin('calificacion', function($join){
                                    $join->on('presentacion.pres_id', '=', 'calificacion.pres_id');
                                                             
                     })
                     ->where('concurso_id',$concurso)
                    /* ->where(function($q) use ($jurado){
                        $q->where('calificacion.jurado_id', '<>',$jurado)
                          ->orWhereNull("calificacion.calif_id");
                     })*/
                    ->where('status','Pendiente')
                    ->whereNotIn('presentacion.pres_id', function($q) use ($jurado){
                        $q->select('pres_id')->from('calificacion')->where('jurado_id',$jurado);
                    })
                    ->groupBy('presentacion.pres_id')
                    ->orderBy('presentacion.pres_id')
                    ->get();
                    //->toSql();

                //return $data;
 
        $datatable= collect();

        foreach ($data as $item) {

             $participante=$item->participantes[0]->participante_name.', '.
             \Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($item->participantes[0]->participante_fechanac)).' Años';
           
             $datatable->push(
                [
                   "id"=>$item->pres_id,
                   "participante"=>($item->categoria_id!=1)?$item->pres_id:$participante,
                   "tecnica"=>substr($item->tecnica->tecnica_name, 6),
                   "variacion"=>$item->variacion->variacion_name.' ('.$item->pres_tiempo.')',
                   "categoria"=>$item->categoria->categoria_name,
                   "division"=>$item->division->division_name,
                   "turno"=>$item->pres_turno,
                   "lugar"=>$item->pres_lugar,
                   "turno_lugar"=>$item->pres_turno.'/'.$item->pres_lugar,
                   "fecha"=>$item->pres_fecha,
                   "fecha_format"=>Carbon::parse($item->pres_fecha)->format('d/M'),
                   "status"=>$item->status,
                   "link"=>$item->pres_link,

                ]);
         
        }

        return $datatable;

    }

    public function scopeDataResultados($query,$concurso) {

        $data=$query->select(DB::raw("presentacion.*"))
                ->with('categoria')
                ->with('variacion')
                ->with('division')
                ->with('tecnica')
                ->with('participantes')
                ->join('calificacion', function($join){
                    $join->on('presentacion.pres_id', '=', 'calificacion.pres_id');
                                             
                 })
                ->where('concurso_id',$concurso)
                ->groupBy('presentacion.pres_id')
                ->get();

        $datatable= collect();



        foreach ($data as $item) {

           // var_dump($item);

             $TOTALES=Calificacion::select(DB::raw("ROUND(SUM(puntaje.puntaje)/COUNT(calificacion.calif_id),2) AS puntaje"))
            ->join('calificacion_puntaje as puntaje', 'calificacion.calif_id', '=', 'puntaje.calif_id')
            ->where('pres_id',$item->pres_id)
            ->groupBy('calificacion.pres_id')
            ->first()['puntaje'];


            /*puntuacion jurado*/

            $subtotales = Calificacion::select(DB::raw("string_agg(subtotales::character varying,' - ') as subtotales"))
            ->from(DB::raw('(select ROUND(SUM(puntaje.puntaje)/COUNT(puntaje.calif_id),2) AS subtotales 
                            from calificacion 
                            inner join calificacion_puntaje as puntaje on calificacion.calif_id = puntaje.calif_id
                            where pres_id = '.$item->pres_id.'
                            group by calificacion.calif_id
                        ) AS totales'))
            ->first()['subtotales'];


             $participante=$item->participantes[0]->participante_name.', '.
             \Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($item->participantes[0]->participante_fechanac)).' Años';

             $alias=array(
                 'PRE-INFANTIL A'=>'PRE-INF A',
                 'PRE-INFANTIL A'=>'PRE-INF B',
                 'INFANTIL'=>'INFANTIL',
                 'JUVENIL'=>'JUVENIL',
                 'AVANZADO'=>'AVANZADO',
                 'MASTER'=>'MASTER',
                );

             $datatable->push(
                [
                   "id"=>$item->pres_id,
                   "participante"=>($item->categoria_id!=1)?$item->pres_id:$participante,
                   "tecnica"=>substr($item->tecnica->tecnica_name, 6),
                   "variacion"=>$item->variacion->variacion_name.' ('.$item->pres_tiempo.')',
                   "categoria"=>$item->categoria->categoria_name,
                   "division"=>$item->division->division_name,
                   "turno"=>$item->pres_turno,
                   "lugar"=>$item->pres_lugar,
                   "lugar_short"=>str_limit($item->pres_lugar,6),
                   "turno_lugar"=>$item->pres_turno.'/'.$item->pres_lugar,
                   "fecha"=>$item->pres_fecha,
                   "fecha_format"=>Carbon::parse($item->pres_fecha)->format('d/M'),
                   "status"=>$item->status,
                   "link"=>$item->pres_link,
                   "total"=>$TOTALES,
                   "puntaje"=>$subtotales,
                ]);
         
        }

        return $datatable;

    }

    public function scopeDataPresentaciones($query,$concurso=null) {

        $data=$query->with('categoria')
                ->with('variacion')
                ->with('division')
                ->with('tecnica')
                ->with('participantes')
                ->get();

        if($concurso){
            $data->where('concurso_id',$concurso);
        }

        $datatable= collect();

        foreach ($data as $item) {

             $participante=$item->participantes[0]->participante_name.', '.
             \Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($item->participantes[0]->participante_fechanac)).' Años';
           
             $datatable->push(
                [
                   "id"=>$item->pres_id,
                   "participante"=>($item->categoria_id!=1)?$item->pres_id:$participante,
                   "tecnica"=>substr($item->tecnica->tecnica_name, 6),
                   "variacion"=>$item->variacion->variacion_name.' ('.$item->pres_tiempo.')',
                   "categoria"=>$item->categoria->categoria_name,
                   "division"=>$item->division->division_name,
                   "turno"=>$item->pres_turno,
                   "lugar"=>$item->pres_lugar,
                   "turno_lugar"=>$item->pres_turno.'/'.$item->pres_lugar,
                   "fecha_format"=>Carbon::parse($item->pres_fecha)->format('d/M'),
                   "fecha"=>$item->pres_fecha,
                   "status"=>$item->status,
                   "concurso"=>$item->concurso->concurso_name,
                   "link"=>$item->pres_link,
                   "academia"=>$item->participantes[0]->academia->academia_name,
                ]);
         
        }

        return $datatable;

    }

   
}
