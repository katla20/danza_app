<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ConcursoConsideraciones extends Model
{
    protected $table = 'concurso_consideraciones';
    protected $primaryKey = 'concurso_consideraciones_id';
    protected $fillable = array(
        'concurso_id',
        'consideracion_id',
    );
    public $timestamps = true;


}
