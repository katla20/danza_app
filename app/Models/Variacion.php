<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variacion extends Model
{
    protected $table = 'variacion';
    protected $primaryKey = 'variacion_id';

    protected $fillable = array(
        'variacion_name',
        'variacion_autor'
    );
}
