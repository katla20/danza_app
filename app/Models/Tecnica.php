<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tecnica extends Model
{
    protected $table = 'tecnica';
    protected $primaryKey = 'tecnica_id';
}
