<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jurado extends Model
{
    protected $table = 'jurado';
    protected $primaryKey = 'jurado_id';

    protected $fillable = array(
        'jurado_name',
        'academia_id',
        'user_id'
    );
    public $timestamps = true;
}
