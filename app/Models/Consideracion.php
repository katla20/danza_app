<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consideracion extends Model
{
    protected $table = 'consideracion';
    protected $primaryKey = 'consideracion_id';

    protected $fillable = array(
        'consideracion_name',
        'categorias'
    );

    protected $casts = [
        'categorias' => 'array',
    ];

    public $timestamps = true;

    public function concurso()
    {
        return $this->belongsToMany('App\Models\Concurso', 'concurso_consideraciones', 'consideracion_id','concurso_id');
    }


    public function scopeByCategory($query, $category)
    {
        $categories = [];

        $aspectos=Consideracion::all();
        foreach ($aspectos as $aspect) {
            if (in_array($category,$aspect->categorias)) {
                $categories[] = $aspect;
            }
        }

        return $categories;

    }

    


}
