<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Academia;
use App\Models\Presentacion;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function home(Request $request){

        $status=Presentacion::Dashboard();

        $widgets=['Pendiente'=>0,'Ejecutada'=>0,'Cancelada'=>0];
        $totales=0;

        foreach ($status as $value) {
                $widgets[$value->status]=$value->totales;
                $totales+=$value->totales;
        }

        $widgets['All']=$totales;

        //dd($widgets);

        $request->user()->authorizeRoles(['jurado', 'admin']);
        $datos=Presentacion::List()->get();

        return view('layouts.index',
        [
            'datos'=>$datos,
            'widgets'=>$widgets
        ]);
    }

    public function index()
    {
        $users = User::orderBy('id', 'ASC')->paginate();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $academies = Academia::get();
        return view('users.create', compact('academies'));
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => 3
        ]);

        DB::table('jurado')->insert([
            'jurado_name' => $request->name,
            'academia_id' => $request->academia_id,
            'status' => true,
            'user_id' => $user->id
        ]);

        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);

        $academies = Academia::get();
        return view('users.edit', compact('user', 'academies'));
    }

    public function update(Request $request, $id)
    {

        $user = User::find($id);

        $user->update($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        DB::table('jurado')->where('user_id', $id)->update([
            'jurado_name' => $request->name,
            'academia_id' => $request->academia_id
        ]);

        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = User::find($id)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('jurado')->where('academia_id', $id)->delete();

        return redirect()->route('users.index');
    }
}
