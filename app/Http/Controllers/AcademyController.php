<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Models\Academia;

class AcademyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $academies = Academia::orderBy('academia_id', 'ASC')->paginate();

        return view('academies.index', compact('academies'));
    }

    public function show($id)
    {
        $academy = Academia::find($id);

        return view('academies.show', compact('academy'));
    }

    public function create()
    {
        return view('academies.create');
    }

    public function store(Request $request)
    {
        $academy = Academia::create($request->all());
        $academy->save();

        return redirect()->route('academies.index');
    }

    public function edit($id)
    {
        $academy = Academia::find($id);

        return view('academies.edit', compact('academy'));
    }

    public function update(Request $request, $id)
    {

        $academy = Academia::find($id);

        $academy->update($request->all());
        $academy->save();

        return redirect()->route('academies.index');
    }

    public function destroy($id)
    {
        $academy = Academia::find($id)->delete();

        return redirect()->route('academies.index');
    }
}
