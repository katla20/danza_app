<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;//Auth::user()->id;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\Datatables\Datatables;
use App\Mail\sendEmailMailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

use App\User;

use App\Models\Concurso;
use App\Models\Calificacion;
use App\Models\Presentacion;
use App\Models\CalificacionPuntaje;
use App\Models\Consideracion;
use App\Models\Jurado;
use App\Models\Categoria;
use App\Models\Division;
use App\Models\Tecnica;



class CalificacionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('calificaciones.index',[
            'flt_tecnicas'=>Tecnica::all(),
            'flt_categorias'=>Categoria::all(),
            'flt_divisiones'=>Division::all(),
            'flt_fechas'=>Presentacion::FiltroFechas()->get(),
            'flt_turnos'=>Presentacion::FiltroTurnos()->get(),
            'flt_lugar'=>Presentacion::FiltroLugares()->get(),
        ]);
    }

    public function datatable(Request $request,$concurso){

        $user = Auth::user();
        $jurado=Jurado::where('user_id',$user->id)->first();
        if(!$jurado){
            return response()->json('no tienes acceso a esta informacion');

        }

        $data=Presentacion::DataCalificaciones($concurso,$jurado->jurado_id);

        //dd($data);

        return datatables()
        ->collection($data)
        ->filter(function ($instance) use ($request) {

            if ($request->has('fecha')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['fecha'], $request->get('fecha')) ? true : false;
                });
            }

            if ($request->has('tecnica')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['tecnica']== trim($request->get('tecnica'))) ? true : false;
                });
            }

            if ($request->has('categoria')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['categoria'], $request->get('categoria')) ? true : false;
                });
            }

            if ($request->has('turno')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['turno'], $request->get('turno')) ? true : false;
                });
            }

            if ($request->has('lugar')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['lugar'], $request->get('lugar')) ? true : false;
                });
            }

            if ($request->has('division')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                     return ($row['division']== trim($request->get('division'))) ? true : false;
                    //return Str::contains($row['division'], $request->get('division')) ? true : false;
                });
            }
        })
         /*->filterColumn('fullname', function($query, $keyword) {
            $sql = "CONCAT(users.first_name,'-',users.last_name)  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })//aplicar filtro avanzados por ejemplo participantes de un grupo
        */
        ->addColumn('action', function ($data) {
            return '<button 
            type="button"
            data-request="{{$request->all()}}"
            class="btn btn-success" calificar_button" 
            data-dismiss="modal" 
            aria-hidden="true"
            title="Calificar"
            data-toggle="modal" 
            data-target="#calificar-modal"
            onClick="openModal('.$data['id'].')"
            >votar</button>';

            /*   return '<button 
            type="button"
            data-request="{{$request->all()}}"
            class="btn btn-success btn-circle calificar_button" 
            data-dismiss="modal" 
            aria-hidden="true"
            title="Calificar"
            data-toggle="modal" 
            data-target="#calificar-modal"
            onClick="openModal('.$data['id'].')"
            ><i class="fa fa-play"></i></button>';*/
        })->editColumn('id', '#{{$id}}')
        ->toJson();

    }

    public function resultados()
    {     
        return view('calificaciones.resultados',
        [
            'flt_tecnicas'=>Tecnica::all(),
            'flt_categorias'=>Categoria::all(),
            'flt_divisiones'=>Division::all(),
            'flt_fechas'=>Presentacion::FiltroFechas()->get(),
            'flt_turnos'=>Presentacion::FiltroTurnos()->get(),
            'flt_lugar'=>Presentacion::FiltroLugares()->get(),
        ]);
    }

    public function resultadosGrandPrix()
    {     
        return view('calificaciones.resultados-grandprix',[
            'flt_divisiones'=>Division::all(),
            'flt_fechas'=>Presentacion::FiltroFechas()->get(),
            'flt_tecnicas'=>Tecnica::all(),
        ]);
    }

    public function print_result(){

        
        $data=Presentacion::where('presentacion.status','Ejecutada')
                        ->with('division')
                        ->with('categoria')
                        ->where('concurso_id',2)->get();
        
        $i=0;
        foreach ($data as $key => $value) {


            $calificaciones=Calificacion::select(DB::raw("calificacion.jurado_id,calificacion.pres_id,ROUND(SUM(puntaje.puntaje)/COUNT(puntaje.puntaje),2) AS puntaje"))
                    ->join('calificacion_puntaje as puntaje', 'calificacion.calif_id', '=', 'puntaje.calif_id')
                    ->join('jurado', 'calificacion.jurado_id', '=', 'jurado.jurado_id')
                    ->where('pres_id',$value->pres_id)
                    ->groupBy('calificacion.pres_id','calificacion.jurado_id')
                    ->get()[0];

            $data[$i]->calificaciones[0]=$calificaciones;

         $i++;
        }

        dd($data);

        $view = \View::make('print.resultados-totales', compact('data'))->render();
        $pdf=PDF::loadHTML($view)->setPaper('a4', 'landscape');//cambiando la horientacion al papel

        return PDF::loadView("print.resultados-totales", compact('data'));
        //return $pdf->download("pagos_programados_{$balances->pay_id}.pdf");
    }



    public function ganadores($concurso){

        $data=Presentacion::where('presentacion.status','Ejecutada')
        ->with('division')
        ->with('categoria')
        ->where('concurso_id',$concurso)->get();
        dd($data);
    }
    
    public function grandPrix()
    {     
        return view('calificaciones.grandprix',[
            'flt_tecnicas'=>Tecnica::all(),
            'flt_categorias'=>Categoria::all(),
            'flt_divisiones'=>Division::all(),
            'flt_fechas'=>Presentacion::FiltroFechas()->get(),
            'flt_turnos'=>Presentacion::FiltroTurnos()->get(),
            'flt_lugar'=>Presentacion::FiltroLugares()->get(),
        ]);
    }

    public function calificar($id)
    {    

        $datos=Presentacion::GetById($id)->first();

        return view('calificaciones.calificar',
        [
            'datos'=>$datos,
            'items'=>Consideracion::ByCategory($datos->categoria_id)
        ]);
    }

    public function test($id)
    {    
        $datos=Presentacion::GetById($id)->first();
        $datos['consideraciones']=Consideracion::ByCategory($datos->categoria_id);
        return response()->json($datos);

    }

    public function testmodal($id)
    {    
        $datos=Presentacion::GetById($id)->first();
       // dd($datos);
        return view('calificaciones.test',
        [
            'datos'=>$datos,
            'items'=>Consideracion::ByCategory($datos->categoria_id)
        ]);

    }

    public function detalleCalificacion($pres_id){

        $datos=$this->queryResultados($pres_id);
        //dd($datos);

        return view('calificaciones.detalle',
        [
            'datos'=>$datos,
        ]);

    }

    public function queryResultados($pres_id){

        $datos=Presentacion::GetById($pres_id)->first();

        $totalP=Calificacion::select(DB::raw("ROUND(SUM(puntaje.puntaje)/COUNT(calificacion.calif_id),2) AS puntaje"))
        ->join('calificacion_puntaje as puntaje', 'calificacion.calif_id', '=', 'puntaje.calif_id')
        ->where('pres_id',$datos->pres_id)
        ->groupBy('calificacion.pres_id')
        ->first()['puntaje'];

        $datos->total=$totalP;
        

        $calificaciones=Calificacion::with('jurado')
        ->with('puntajes.consideracion')
        ->where('pres_id',$datos->pres_id)->get();

        if($calificaciones){
            $i=0;

            foreach ($calificaciones as $key => $value) {
                //join con los totalles del sistema
                $totales=CalificacionPuntaje::select(DB::raw("ROUND(SUM(puntaje)/COUNT(puntaje),2) AS puntaje"))
                ->where('calif_id',$value->calif_id)
                ->groupBy('calif_id')
                ->first()['puntaje']; 
                $calificaciones[$i]->total=$totales;

            $i++;
            }

            $datos->calificaciones[0]=$calificaciones;

        }

            return $datos;

    }


    public function datatableResultados(Request $request,$concurso){

        //lista de todos los resultados 

        $data=Presentacion::DataResultados($concurso);

        //dd($data);
  
        return datatables()
        ->collection($data)
        ->filter(function ($instance) use ($request) {

            if ($request->has('fecha')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['fecha'], $request->get('fecha')) ? true : false;
                });
            }

            if ($request->has('tecnica')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['tecnica']== trim($request->get('tecnica'))) ? true : false;
                });
            }

            if ($request->has('categoria')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['categoria'], $request->get('categoria')) ? true : false;
                });
            }

            if ($request->has('turno')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['turno'], $request->get('turno')) ? true : false;
                });
            }

            if ($request->has('lugar')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['lugar'], $request->get('lugar')) ? true : false;
                });
            }

            if ($request->has('division')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['division']== trim($request->get('division'))) ? true : false;
                    //return Str::contains($row['division'], $request->get('division')) ? true : false;
                });
            }
        })
        ->addColumn('action', function ($data) {
            return '<a class="btn btn-secondary"
                       href="'. route("calificacion.detalle", $data['id']) .'">
                    <i class="fa fa-eye text-info"></i>
                    </a>';

        })->editColumn('id', '#{{$id}}')
        ->toJson();

    }

    public function saveCalificacion(Request $request)
    {
        
        $inputs = $request->all();
        $user = Auth::user();
        $jurado=Jurado::where('user_id',$user->id)->first();
        if(!$jurado){
            return response()->json("Jurado no autorizado");//esta calificando el administrador
        }

        $calificacion = new Calificacion(
            [
                'calif_fecha' => Carbon::now(),
                'pres_id' => $request->id,
                'jurado_id' => $jurado->jurado_id,
                'calif_observacion' => $request->observaciones,
            ]
        );

        if($calificacion->save()){


            foreach ($inputs as $key => $value) {
                if(strpos($key, 'puntaje')!== false){
                    $puntaje=explode(":", $key)[1];
                    $valor=(!is_null($value))?$value:'0';
                    //$puntajes_array[$puntaje]=$valor;

                    CalificacionPuntaje::create(
                    [
                        'calif_id' => $calificacion->calif_id,
                        'consideracion_id' => $puntaje,
                        'puntaje' => $valor
                    ]);

                }     
            }

            //return response()->json($puntajes_array);

            //query para saber si el numero de votantes es 
            //igual al numero definid en la columna concurso_jurado

           /* $A=Presentacion::with('concurso')->where('pres_id',$request->id)->first();
            $B=Calificacion::where('pres_id',$request->id)->count();

            if($A->concurso->concurso_jurado==$B){
                if(!Presentacion::where('pres_id', $request->id)->update(['status' =>'Ejecutada'])){
                    return response()->json("no se actualizo la presentacion");
                }
            }*/

        }else{
           
        }

        //return response()->json($request->id);

    }


}
