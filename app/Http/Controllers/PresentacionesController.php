<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;//Auth::user()->id;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\Datatables\Datatables;
use App\Mail\sendEmailMailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use App\Models\Presentacion;
use App\Models\Participante;
use Illuminate\Support\Facades\Input;
use App\Models\Division;
use App\Models\Variacion;
use App\Models\Tecnica;
use App\Models\Categoria;
use App\Models\Academia;
use App\Models\Concurso;

//excel library
use Importer;

class PresentacionesController extends Controller
{
    private $presentacion;

    public function __construct($presentacion=null)
    {
        $this->middleware('auth');
        $this->presentacion=new Presentacion();
    }

    public function presentaciones()
    {

        return view('presentaciones.index', [
            'flt_tecnicas'=>Tecnica::all(),
            'flt_categorias'=>Categoria::all(),
            'flt_divisiones'=>Division::all(),
            'flt_fechas'=>Presentacion::FiltroFechas()->get(),
            'flt_turnos'=>Presentacion::FiltroTurnos()->get(),
            'flt_lugar'=>Presentacion::FiltroLugares()->get(),
            'flt_academias'=>Participante::FiltroAcademias()->get(),
            'flt_concurso'=>Concurso::all(),
            
        ]);

   
    }

    public function datatable(Request $request){

        $data=Presentacion::DataPresentaciones();
        $icon="";

        return datatables()
        ->collection($data)
        ->filter(function ($instance) use ($request) {

            if ($request->has('fecha')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['fecha'], $request->get('fecha')) ? true : false;
                });
            }

            if ($request->has('tecnica')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['tecnica'], $request->get('tecnica')) ? true : false;
                });
            }

            if ($request->has('categoria')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['categoria'], $request->get('categoria')) ? true : false;
                });
            }

            if ($request->has('turno')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['turno'], $request->get('turno')) ? true : false;
                });
            }

            if ($request->has('lugar')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['lugar'], $request->get('lugar')) ? true : false;
                });
            }

            if ($request->has('division')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['division']== trim($request->get('division'))) ? true : false;
                });
            }

            if ($request->has('academia')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['academia']== trim($request->get('academia'))) ? true : false;
                });
            }

            if ($request->has('concurso')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['concurso']== trim($request->get('concurso'))) ? true : false;
                });
            }

        })
        ->addColumn('action', function ($data) {
            $id=$data['id'];
            return '<a class="btn btn-secondary" href="'. route("presentaciones.detalle", $id) .'"><i class="fa fa-eye text-info"></i></a>';
        })->toJson();
        
    }

    public function detalle($pres_id)
    {

        $datos=Presentacion::GetById($pres_id)->first();
        return view('presentaciones.detalle',[
            'datos'=>$datos,
        ]);
    }

    public function saveExcelDanza() 
    {

        if(request()->hasFile('file')){

            $data=Importer::make('Excel')->load(request()->file('file'))->getCollection();

            //dd($data);
            foreach ($data as $key => $colum) {
              
                if($key == 0) continue;
                if($colum[0]=='') break;

                $nac=Carbon::now()->subYears((int)$colum[2]);//calcular edad

                   // dd($fecha);

                    $presentacion = new Presentacion([
                        'pres_fecha' =>  (trim($colum[5])!="")? trim($colum[5]):Carbon::now(),
                        'variacion_id' => Variacion::firstOrCreate(['variacion_name' => strtoupper(trim($colum[6]))])['variacion_id'],
                        'division_id' =>  Division::where('division_name',trim($colum[10]))->first()['division_id'],
                        'categoria_id' => 1,
                        'concurso_id' => 1,//pura danza
                        'tecnica_id' => Tecnica::where('tecnica_name',trim($colum[7]))->first()['tecnica_id'],
                        'pres_turno' => ($colum[11]!="")? trim($colum[11]):'N/A',
                        'pres_lugar' => strtoupper(trim($colum[12])),
                        'pres_tiempo' => ($colum[8]!="")? trim($colum[8]):'0:00',
                    ]);
    
                   // dd($presentacion);
    
                    $presentacion->save();


                    $part = new Participante([
                        'participante_name' => strtoupper(trim($colum[1])),
                        'participante_identificacion' => trim($colum[3]),
                        'pres_id'=>$presentacion->pres_id,
                        'academia_id' => Academia::firstOrCreate(['academia_name' => strtoupper(trim($colum[9]))])['academia_id'],
                        'participante_fechanac' => $nac->format('d-m-Y'), 
                    ]);
    
                    //dd($part);
                    $presentacion->participantes()->save($part); 
 
            }//fin foreach 
            
            return redirect()->route('presentaciones');

        }else{//fin file exixst
            throw new FileNotFoundException('File not found');
        }

    }

    public function saveExcelGrandPrix() 
    {
        
        //dd(request()->file('file'));
       
        $i=0;
        $data=Importer::make('Excel')->load(request()->file('file'))->getCollection();
       // dd($data);


       if(request()->hasFile('file')){


        foreach ($data as $key => $colum) {

                if($key == 0) continue;
                if($colum[0]=='') break;

                $nac=Carbon::now()->subYears((int)$colum[2]);//calcular edad

                $presentacion = new Presentacion(
                    [
                        'pres_fecha' => (trim($colum[8])!="")? trim($colum[8]):Carbon::now(),
                        'variacion_id' => Variacion::firstOrCreate(['variacion_name' => strtoupper(trim($colum[4]))])['variacion_id'],
                        'division_id' =>  Division::where('division_name',trim($colum[7]))->first()['division_id'],
                        'categoria_id' => 1,
                        'concurso_id' => 2,//pura danza
                        'tecnica_id' => Tecnica::where('tecnica_name',trim($colum[5]))->first()['tecnica_id'],
                        'pres_turno' => ($colum[9]!="")? trim($colum[9]):'N/A',
                        'pres_lugar' => strtoupper(trim('TULIO FEBRES CORDERO')),
                        'pres_tiempo' => ($colum[6]!="")? trim($colum[6]):'0:00',
                    //'pres_link' => trim($colum[1])
                    ]);

                $presentacion->save();

                $part =  new  Participante(
                    [
                        'participante_name' => strtoupper(trim($colum[1])),
                        'participante_identificacion' => trim($colum[3]),
                        'pres_id'=>$presentacion->pres_id,
                        'academia_id' => 1,
                        'participante_fechanac' => $nac->format('d-m-Y'), 
                    ]);

                $presentacion->participantes()->save($part); 

                }

                return redirect()->route('presentaciones');
                
        }else{//fin file exixst
            throw new FileNotFoundException('File not found');
        }

    }

    public function saveExcelGrupos(){

      
        $data=Importer::make('Excel')->load(request()->file('file'))->getCollection();
       // dd($data);
       
        if(request()->hasFile('file')){

            $numero=0;//contador para registrar solo una presentacion por grupo
 
            foreach ($data as $key => $colum) {
 
                if($key == 0) continue;
                if($colum[0]=='') break;

                $nac=Carbon::now()->subYears((int)$colum[3]);//calcular edad

                if($numero!=$colum[0]){

                    //echo "<pre>--presentacion--$numero<pre>";

                    $presentacion = new Presentacion([
                        'pres_fecha' => (trim($colum[5])!="")? trim($colum[5]):Carbon::now(),
                        'variacion_id' => Variacion::firstOrCreate(['variacion_name' => strtoupper(trim($colum[1]))])['variacion_id'],
                        'division_id' =>  Division::where('division_name',trim($colum[9]))->first()['division_id'],
                        'categoria_id' => Categoria::where('categoria_name',trim($colum[10]))->first()['categoria_id'],
                        'concurso_id' => 1,//pura danza
                        'tecnica_id' => Tecnica::where('tecnica_name',trim($colum[6]))->first()['tecnica_id'],
                        'pres_turno' => ($colum[11]!="")? trim($colum[11]):'N/A',
                        'pres_lugar' => strtoupper(trim($colum[12])),
                        'pres_tiempo' => trim($colum[7]),
                    ]);

                    if($presentacion->save()){

                        $participante = new Participante([
                            'participante_name' => strtoupper(trim($colum[2])),
                            'pres_id'=>$presentacion->pres_id,
                            'academia_id' => Academia::firstOrCreate(['academia_name' => strtoupper(trim($colum[8]))])['academia_id'],
                            'participante_fechanac' => $nac->format('d-m-Y'),

                        ]);

                        $participante->save();

                        $this->presentacion=$presentacion;
                        
                    }

                    $numero=$colum[0];

                }else{

                    //echo "<pre>$numero<pre>";

                    $participante = new Participante([
                        'participante_name' => strtoupper(trim($colum[2])),
                        'pres_id'=>$presentacion->pres_id,
                        'academia_id' => Academia::firstOrCreate(['academia_name' => strtoupper(trim($colum[8]))])['academia_id'],
                        'participante_fechanac' => $nac->format('d-m-Y'),

                    ]);

                    $participante->save();

                }

            }//foreach ($data as $key => $colum) {

            return redirect()->route('presentaciones');
                
        }else{//fin file exixst
            throw new FileNotFoundException('File not found');
        }
        
    }        
        
  
}