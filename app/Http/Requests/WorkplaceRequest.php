<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkplaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            'work_name' => 'required|unique:workplace|max:255'
        ];
    }

    public function messages()
    {
        return [
            'work_name.required' => 'Ingrese un cargo',
            'work_name.unique' => 'El cargo ya existe!!',
            'work_name.max' => 'Cargo no debe superar los 255 caracteres',
        ];
    }
}
