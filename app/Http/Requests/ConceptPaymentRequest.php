<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConceptPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'concept_name' => 'required|unique:concept_payment|max:255'
        ];
    }

    public function messages()
    {
        return [
            'concept_name.required' => 'Ingrese un concepto de pago',
            'concept_name.unique' => 'El concepto ya existe!!',
            'concept_name.max' => 'Concepto no debe superar los 255 caracteres',
        ];
    }
}
