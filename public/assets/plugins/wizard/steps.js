var form = $(".validation-wizard").show();

$(".validation-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "fade"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Enviar Datos",
        next: 'Siguiente »',
        previous: '« Atras'

    }
    , onStepChanging: function (event, currentIndex, newIndex) {
        return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
    }
    , onFinishing: function (event, currentIndex) {
        return form.validate().settings.ignore = ":disabled", form.valid()
    },
     onFinished: function (event, currentIndex) {
         swal({
            title: "Empleado Creado!",
            text: "Junto a la creacion se creo un usuario en en sistema el cual podrá acceder con su correo y la contraseña que cargo en el formulario.",
            icon: "success",
            button: "Continuar",
          });
         var form = $(this);
         form.submit();      
    }
}),
$(".validation-wizard").validate({
    ignore: "input[type=hidden]"
    , errorClass: "text-danger"
    , successClass: "text-success"
    , highlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , unhighlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , errorPlacement: function (error, element) {
        error.insertAfter(element)
    },
    lang: 'es',
    rules: {
      entity_name: "required",
      entity_lastname: "required",
      employee_wallet: "required",
      workplace_id:"required",
      employee_wallet_api: "required",
      entity_identity: { required: true, number: true },
      employee_salary: { required: true, number: true },
      entity_email: { required: true, email: true },
      entity_password: { required: true, minlength: 5 }
    },
    messages: {
      entity_name: "Se requiere",
      entity_lastname: "Se requiere",
      employee_wallet: "Se requiere",
      workplace_id: "Seleccione",
      employee_wallet_api: "Seleccione",
      entity_password: {
        required: "Se requiere",
        minlength: "Su contraseña debe tener al menos 5 caracteres de largo"
      },
      entity_email: {
        required: "Se requiere",
        email: "Introduzca un correo valido"
      },
      employee_salary: { required: "Se requiere", number: "solo numeros" },
      entity_identity: { required: "Se requiere", number: "solo numeros" }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
        form.submit();
    }
});