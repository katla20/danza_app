// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $(".validation-wizard").validate({
    // Specify validation rules
    lang: 'es',
    rules: {
      entity_name: "required",
      entity_lastname: "required",
      employee_wallet: "required",
      workplace_id:"required",
      employee_wallet_api: "required",
      entity_identity: { required: true, number: true },
      employee_salary: { required: true, number: true },
      entity_email: { required: true, email: true },
      entity_password: { required: true, minlength: 5 }
    },
    messages: {
      entity_name: "Se requiere",
      entity_lastname: "Se requiere",
      employee_wallet: "Se requiere",
      workplace_id: "Seleccione",
      employee_wallet_api: "Seleccione",
      entity_password: {
        required: "Se requiere",
        minlength: "Su contraseña debe tener al menos 5 caracteres de largo"
      },
      entity_email: {
        required: "Se requiere",
        email: "Introduzca un correo valido"
      },
      employee_salary: { required: "Se requiere", number: "solo numeros" },
      entity_identity: { required: "Se requiere", number: "solo numeros" }
    },
    submitHandler: function(form) {
      // in the "action" attribute of the form when valid // Specify validation error messages // Make sure the form is submitted to the destination defined
      form.submit();
    }
  });
});

/*rules: {
  "resume[zip_code]": {
       required: true,
       minlength: 5,
       digits: true
   },
  contact: {
      required: true,
      email: {
        depends: function(element) {
          return $("#contactform_email").is(":checked");
        }
      }
    }

},
messages: {
  "resume[zip_code]": {
       required: "this field is required",
       minlength: "this field must contain at least {0} characters",
       digits: "this field can only contain numbers"
   }
}*/
