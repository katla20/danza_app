<?php

use Illuminate\Database\Seeder;
use App\Models\Presentacion;


class PresentacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //pura danza

        $role = new Presentacion();
        $role->concurso_id = 1;
        $role->pres_fecha = '15/03/2019';
        $role->variacion_id = 1;
        $role->pres_tiempo='2:50';
        $role->division_id = 1;
        $role->pres_lugar = 'MAESTRANZA';
        $role->pres_turno = 'TARDE';
        $role->categoria_id = 1;
        $role->tecnica_id=1;
        $role->save();

        $role = new Presentacion();
        $role->concurso_id = 1;
        $role->pres_fecha = '15/03/2019';
        $role->variacion_id = 2;
        $role->pres_tiempo='2:50';
        $role->pres_turno = 'MAÑANA';
        $role->division_id = 1;
        $role->pres_lugar = 'TEATRO';
        $role->categoria_id = 2;
        $role->tecnica_id=2;
        $role->save();

        $role = new Presentacion();
        $role->concurso_id = 1;
        $role->pres_fecha = '17/03/2019';
        $role->variacion_id = 3;
        $role->pres_tiempo='1:50';
        $role->pres_lugar = 'TEATRO';
        $role->pres_turno = 'MAÑANA';
        $role->division_id = 1;
        $role->categoria_id = 3;
        $role->tecnica_id=2;
        $role->save();

        //grand prix

        $role = new Presentacion();
        $role->concurso_id = 2;
        $role->pres_fecha = '15/03/2019';
        $role->variacion_id = 3;
        $role->pres_tiempo='2:50';
        $role->division_id = 3;
        $role->pres_turno = 'TARDE';
        $role->pres_lugar = 'N/A';
        $role->categoria_id = 1;
        $role->tecnica_id=1;
        $role->pres_link="https://www.youtube.com/watch?v=v0Ebt5u-zwI";

        $role->save();

        $role = new Presentacion();
        $role->concurso_id = 2;
        $role->pres_fecha = '15/03/2019';
        $role->variacion_id = 1;
        $role->pres_tiempo='2:50';
        $role->division_id = 2;
        $role->categoria_id = 1;
        $role->pres_turno = 'MAÑANA';
        $role->pres_lugar = 'N/A';
        $role->pres_link="https://www.youtube.com/watch?v=Ve4b4vmRqpM&feature=youtu.be";	

        $role->tecnica_id=3;
        $role->save();

    }
}
