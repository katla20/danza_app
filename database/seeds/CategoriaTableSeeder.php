<?php

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Categoria();
        $role->categoria_name = 'SOLO';
        $role->save();

        $role = new Categoria();
        $role->categoria_name = 'PAS DE DEUX';
        $role->save();

        $role = new Categoria();
        $role->categoria_name = 'DUO';
        $role->save();

        $role = new Categoria();
        $role->categoria_name = 'GRUPO A';
        $role->save();

        $role = new Categoria();
        $role->categoria_name = 'GRUPO B';
        $role->save();
    }
}
