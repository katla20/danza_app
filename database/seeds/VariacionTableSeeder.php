<?php

use Illuminate\Database\Seeder;
use App\Models\Variacion;

class VariacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Variacion();
        $role->variacion_name = 'Feels';
        $role->save();

        $role = new Variacion();
        $role->variacion_name = 'Bangarang';
        $role->save();

        $role = new Variacion();
        $role->variacion_name = 'Animals';
        $role->save();

       
    }
}
