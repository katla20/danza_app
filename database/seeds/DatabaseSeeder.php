<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(RoleTableSeeder::class);

        $this->call(UserTableSeeder::class);

        $this->call(CategoriaTableSeeder::class);

        $this->call(DivisionTableSeeder::class);

        $this->call(VariacionTableSeeder::class);

        $this->call(AcademiaTableSeeder::class);

        $this->call(ConsideracionTableSeeder::class);

        $this->call(TecnicaTableSeeder::class);

        $this->call(ConcursoTableSeeder::class);

        $this->call(JuradoTableSeeder::class);

        $this->call(ConcursoConsideracionesTableSeeder::class);

       // $this->call(PresentacionTableSeeder::class);

       // $this->call(ParticipanteTableSeeder::class);

        
    }
}
