<?php

use Illuminate\Database\Seeder;
use App\Models\Division;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Division();
        $role->division_name = 'PRE-INFANTIL A';
        $role->save();

        $role = new Division();
        $role->division_name = 'PRE-INFANTIL B';
        $role->save();

        $role = new Division();
        $role->division_name = 'INFANTIL';
        $role->save();

        $role = new Division();
        $role->division_name = 'JUVENIL';
        $role->save();

        $role = new Division();
        $role->division_name = 'AVANZADO';
        $role->save();

        $role = new Division();
        $role->division_name = 'MASTER';
        $role->save();
    }
}
