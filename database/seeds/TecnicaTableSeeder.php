<?php

use Illuminate\Database\Seeder;
use App\Models\Tecnica;

class TecnicaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tec = new Tecnica();
        $tec->tecnica_name = 'DANZA CONTEMPORANEA';
        $tec->save();

        $tec = new Tecnica();
        $tec->tecnica_name = 'DANZA NEOCLASICA';
        $tec->save();

        $tec = new Tecnica();
        $tec->tecnica_name = 'DANZA CLASICA';
        $tec->save();

        
    }
}
