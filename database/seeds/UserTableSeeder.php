<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $role_user = Role::where('name', 'jurado')->first();
        $role_admin = Role::where('name', 'admin')->first();

        $user = new User();
        $user->name = 'Jurado A';
        $user->username = 'Jurado A';
        $user->email = 'juradoa@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado B';
        $user->username = 'Jurado B';
        $user->email = 'juradob@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado C';
        $user->username = 'Jurado C';
        $user->email = 'juradoc@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado D';
        $user->username = 'Jurado D';
        $user->email = 'juradod@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado E';
        $user->username = 'Jurado E';
        $user->email = 'juradoe@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado F';
        $user->username = 'Jurado F';
        $user->email = 'juradof@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado G';
        $user->username = 'Jurado G';
        $user->email = 'juradog@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Jurado H';
        $user->username = 'Jurado H';
        $user->email = 'juradoh@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Admin';
        $user->username = 'admin';
        $user->email = 'admin@info.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_admin);
     }
}
