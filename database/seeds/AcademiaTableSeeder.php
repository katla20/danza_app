<?php

use Illuminate\Database\Seeder;
use App\Models\Academia;

class AcademiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Academia();
        $role->academia_name = 'INDEPENDIENTE';
        $role->academia_correo = 'correo@info.com';
        $role->ciudad = 'Maracay';
        $role->pais = 'Venezuela';
        $role->save();

    }
}
