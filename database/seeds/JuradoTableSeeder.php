<?php

use Illuminate\Database\Seeder;
use App\Models\Jurado;

class JuradoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado';
        $role->academia_id = 1;
        $role->user_id = 1;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado B';
        $role->academia_id = 1;
        $role->user_id = 2 ;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado C';
        $role->academia_id = 1;
        $role->user_id = 3 ;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado D';
        $role->academia_id = 1;
        $role->user_id = 4 ;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado E';
        $role->academia_id = 1;
        $role->user_id = 5 ;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado F';
        $role->academia_id = 1;
        $role->user_id = 6 ;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado G';
        $role->academia_id = 1;
        $role->user_id = 7 ;
        $role->save();

        $role = new Jurado();
        $role->jurado_name = 'Jurado Invitado G';
        $role->academia_id = 1;
        $role->user_id = 8 ;
        $role->save();
    }
}
