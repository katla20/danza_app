<?php

use Illuminate\Database\Seeder;
use App\Models\Concurso;

class ConcursoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Concurso();
        $role->concurso_name = 'Pura Danza 2019';
        $role->concurso_tipo = 'Ballet';
        $role->concurso_jurado = '3';
        $role->concurso_fecha = '25/03/2019';
        $role->ciudad = 'San Cristobal';
        $role->pais = 'Venezuela';
        $role->direccion = 'Sin especificar';
        $role->save();

        $role = new Concurso();
        $role->concurso_name = 'Gran Prix';
        $role->concurso_tipo = 'Ballet';
        $role->concurso_jurado = '3';
        $role->concurso_fecha = '25/03/2019';
        $role->ciudad = 'San Cristobal';
        $role->pais = 'Venezuela';
        $role->direccion = 'Sin especificar';
        $role->save();

    }
}
