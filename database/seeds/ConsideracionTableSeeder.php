<?php

use Illuminate\Database\Seeder;
use App\Models\Consideracion;

class ConsideracionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Consideracion();
        $role->consideracion_name = 'Técnica';
        $role->categorias = array(1,2,3,4,5);
        $role->save();

        $role = new Consideracion();
        $role->consideracion_name = 'Ejecución ';
        $role->categorias = array(1,2,3);
        $role->save();

        $role = new Consideracion();
        $role->consideracion_name = 'Presencia escénica';
        $role->categorias = array(1,2,3);
        $role->save();

        $role = new Consideracion();
        $role->consideracion_name = 'Ritmo y musicalidad';
        $role->categorias = array(1,2,3);
        $role->save();

        $role = new Consideracion();
        $role->consideracion_name = 'Coreografía ';
        $role->categorias = array(4,5);
        $role->save();

        $role = new Consideracion();
        $role->consideracion_name = 'Presentación';
        $role->categorias = array(4,5);
        $role->save();

        $role = new Consideracion();
        $role->consideracion_name = 'Sincronización';
        $role->categorias = array(4,5);
        $role->save();

    }
}
