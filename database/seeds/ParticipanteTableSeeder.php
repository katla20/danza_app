<?php

use Illuminate\Database\Seeder;
use App\Models\Participante;

class ParticipanteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = new Participante();
        $role->participante_name = 'Maria Gonzalez';
        $role->academia_id = 1;
        $role->pres_id = 1;
        $role->participante_fechanac = '21/03/2008';
        $role->participante_direccion = 'sin especificar';
        $role->save();
       
        $role = new Participante();
        $role->participante_name = 'Lorena Lopez';
        $role->academia_id = 1;
        $role->pres_id = 2;
        $role->participante_fechanac = '21/02/2007';
        $role->participante_direccion = 'sin especificar';
        $role->save();

        $role = new Participante();
        $role->participante_name = 'Lina Alvarado';
        $role->academia_id = 1;
        $role->pres_id = 2;
        $role->participante_fechanac = '01/08/2008';
        $role->participante_direccion = 'sin especificar';
        $role->save();

        $role = new Participante();
        $role->participante_name = 'Keyla Bullon';
        $role->academia_id = 1;
        $role->pres_id = 3;
        $role->participante_fechanac = '21/02/2007';
        $role->participante_direccion = 'sin especificar';
        $role->save();

        $role = new Participante();
        $role->participante_name = 'Jose Tovar';
        $role->academia_id = 1;
        $role->pres_id = 3;
        $role->participante_fechanac = '01/08/2008';
        $role->participante_direccion = 'sin especificar';
        $role->save();

        $role = new Participante();
        $role->participante_name = 'Reinando Solar';
        $role->academia_id = 1;
        $role->pres_id = 3;
        $role->participante_fechanac = '01/08/2008';
        $role->participante_direccion = 'sin especificar';
        $role->save();


        //GRAND PRIX
        $role = new Participante();
        $role->participante_name = 'Alejandra Rivas';
        $role->academia_id = 1;
        $role->pres_id = 4;
        $role->participante_fechanac = '07/10/2005';
        $role->participante_direccion = 'sin especificar';
        $role->save();

        $role = new Participante();
        $role->participante_name = 'Helen ri';
        $role->academia_id = 1;
        $role->pres_id = 5;
        $role->participante_fechanac = '21/03/2010';
        $role->participante_direccion = 'sin especificar';
        $role->save();

 
    }
}
