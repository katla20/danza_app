<?php

use Illuminate\Database\Seeder;
use App\Models\ConcursoConsideraciones;
use App\Models\Concurso;
use App\Models\Consideracion;

class ConcursoConsideracionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $concurso = Concurso::first();
        $consideraciones = Consideracion::all();

        
        foreach($consideraciones as $item){
            
            $concursoConsideraciones = new ConcursoConsideraciones();
            $concursoConsideraciones->concurso_id = $concurso->concurso_id;
            $concursoConsideraciones->consideracion_id = $item->consideracion_id;

            $concursoConsideraciones->save();
        }

    }
}
