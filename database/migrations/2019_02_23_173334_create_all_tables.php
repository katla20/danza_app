<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //tablas basicas 

        Schema::create('categoria', function (Blueprint $table) {
            $table->increments('categoria_id');
            //$table->morphs('categoria');
            $table->string('categoria_name', 250)->nullable(false);
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('division', function (Blueprint $table) {
            $table->increments('division_id');
            $table->string('division_name', 250)->nullable(false);
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('variacion', function (Blueprint $table) {
            $table->increments('variacion_id');
            $table->string('variacion_name', 250)->nullable(false);
            $table->string('variacion_autor', 250)->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('tecnica', function (Blueprint $table) {
            $table->increments('tecnica_id');
            $table->string('tecnica_name', 250)->nullable(false);
            $table->boolean('status')->default(true);
            $table->timestamps();
        });


        Schema::create('academia', function (Blueprint $table) {
            $table->increments('academia_id');
            $table->string('academia_name', 250)->nullable(false);
            $table->string('academia_correo', 250)->nullable();
            $table->string('academia_telefono', 250)->nullable();
            $table->string('academia_directora', 250)->nullable();
            $table->string('academia_division', 250)->nullable();
            $table->string('pais', 250)->nullable();
            $table->string('estado', 250)->nullable();
            $table->string('ciudad', 250)->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('concurso', function (Blueprint $table) {
            $table->increments('concurso_id');
            $table->string('concurso_name', 250)->nullable(false);
            $table->string('concurso_tipo', 200)->nullable();
            $table->string('concurso_jurado', 250)->nullable();
           // $table->json('concurso_jurado')->nullable();
            $table->date('concurso_fecha')->nullable(false);
            $table->string('ciudad', 250)->nullable();
            $table->string('pais', 250)->nullable();
            $table->string('direccion', 250)->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        
        Schema::create('consideracion', function (Blueprint $table) {
            $table->increments('consideracion_id');
            $table->string('consideracion_name', 250)->nullable(false);
            $table->json('categorias')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('concurso_consideraciones', function (Blueprint $table) {
            $table->increments('concurso_consideraciones_id');

            $table->integer('concurso_id')->unsigned()->nullable(false);
            $table->foreign('concurso_id')
                    ->references('concurso_id')
                    ->on('concurso')
                    ->onDelete('cascade');
            
            $table->integer('consideracion_id')->unsigned()->nullable(false);
            $table->foreign('consideracion_id')
                    ->references('consideracion_id')
                    ->on('consideracion')
                    ->onDelete('cascade');
            
            $table->timestamps();
        });



        Schema::create('jurado', function (Blueprint $table) {
            $table->increments('jurado_id');
            $table->string('jurado_name', 350)->nullable(false);

            $table->integer('academia_id')->unsigned()->nullable(false);
            $table->foreign('academia_id')
                    ->references('academia_id')
                    ->on('academia')
                    ->onDelete('cascade');
            
            $table->integer('user_id')->unsigned()->nullable(false);
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        //tablas de dependencia
        Schema::create('presentacion', function (Blueprint $table) {

            $table->increments('pres_id');
            
            $table->date('pres_fecha')->nullable(false);

            $table->integer('variacion_id')->unsigned()->nullable(false);
            $table->foreign('variacion_id')
            ->references('variacion_id')
            ->on('variacion')
            ->onDelete('cascade');

            $table->integer('division_id')->unsigned()->nullable(false);
            $table->foreign('division_id')
            ->references('division_id')
            ->on('division')
            ->onDelete('cascade');

            $table->integer('categoria_id')->unsigned()->nullable(false);
            $table->foreign('categoria_id')
            ->references('categoria_id')
            ->on('categoria')
            ->onDelete('cascade');

            $table->integer('concurso_id')->unsigned()->nullable(false);
            $table->foreign('concurso_id')
            ->references('concurso_id')
            ->on('concurso')
            ->onDelete('cascade');

            $table->integer('tecnica_id')->unsigned()->nullable(false);
            $table->foreign('tecnica_id')
            ->references('tecnica_id')
            ->on('tecnica')
            ->onDelete('cascade');


            $table->string('pres_turno', 25)->nullable();
            $table->string('pres_grupo', 250)->nullable();
            $table->string('pres_lugar', 250)->nullable();
            $table->string('pres_tiempo',6)->nullable();//minutos
            $table->string('pres_link', 700)->nullable();
            $table->text('status')->default('Pendiente');//Pendiente,Ejecutado,Cancelado
            $table->decimal('pres_resultado', 3, 2)->nullable();
            $table->text('pres_observacion')->nullable();

            $table->timestamps();


        });

        Schema::create('participante', function (Blueprint $table) {
            $table->increments('participante_id');
            $table->string('participante_name', 350)->nullable(false);
            $table->string('participante_identificacion', 20)->nullable();
            $table->integer('academia_id')->unsigned()->nullable(false);

            $table->foreign('academia_id')
            ->references('academia_id')
            ->on('academia')
            ->onDelete('cascade');

            $table->integer('pres_id')->unsigned()->nullable(false);
            $table->foreign('pres_id')
            ->references('pres_id')
            ->on('presentacion')
            ->onDelete('cascade');

            $table->date('participante_fechanac')->nullable(false);
            $table->string('participante_direccion', 350)->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

    
        Schema::create('calificacion', function (Blueprint $table) {
            $table->increments('calif_id');
            
            $table->date('calif_fecha')->nullable(false);

            $table->integer('jurado_id')->unsigned()->nullable(false);
            $table->foreign('jurado_id')
            ->references('jurado_id')
            ->on('jurado')
            ->onDelete('cascade');

            $table->integer('pres_id')->unsigned()->nullable(false);
            $table->foreign('pres_id')
            ->references('pres_id')
            ->on('presentacion')
            ->onDelete('cascade');

            $table->text('calif_observacion')->nullable();

            $table->timestamps();
        });

        Schema::create('calificacion_puntaje', function (Blueprint $table) {
            $table->increments('califp_id');

            $table->integer('calif_id')->unsigned()->nullable(false);
            $table->foreign('calif_id')
            ->references('calif_id')
            ->on('calificacion')
            ->onDelete('cascade');

            $table->integer('consideracion_id')->unsigned()->nullable(false);
            $table->foreign('consideracion_id')
            ->references('consideracion_id')
            ->on('consideracion')
            ->onDelete('cascade');

            $table->decimal('puntaje', 4, 2)->nullable(false);


            $table->timestamps();
        });


       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           
        Schema::table('presentacion', function (Blueprint $table) {
            $table->dropForeign('presentacion_concurso_id_foreign');
        });

      
        Schema::table('concurso_consideraciones', function (Blueprint $table) {
            $table->dropForeign('concurso_consideraciones_concurso_id_foreign');
        });

        Schema::table('participante', function (Blueprint $table) {
            $table->dropForeign('participante_pres_id_foreign');
        });

        Schema::table('calificacion', function (Blueprint $table) {
            $table->dropForeign('calificacion_pres_id_foreign');
        });

        Schema::dropIfExists('concurso_consideraciones');

        Schema::dropIfExists('concurso');

        Schema::dropIfExists('calificacion_puntaje');
        Schema::dropIfExists('calificacion');
       
        Schema::dropIfExists('jurado');
        Schema::dropIfExists('participante');

        Schema::dropIfExists('presentacion');

        Schema::dropIfExists('categoria');
        Schema::dropIfExists('division');
        Schema::dropIfExists('variacion');
        Schema::dropIfExists('academia');
        Schema::dropIfExists('consideracion');
        Schema::dropIfExists('tecnica');


        
        

    }

    
}

