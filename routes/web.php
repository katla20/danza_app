<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {


        Route::get('/',['uses'=> 'UserController@home'])->name('home');

        Route::get('/home',['uses'=> 'UserController@home'])->name('home');

        Route::get('/presentaciones',['uses'=> 'PresentacionesController@presentaciones'])->name('presentaciones');

        Route::post('/presentaciones/datatable',['uses'=> 'PresentacionesController@datatable'])->name('presentaciones.datatable');

        Route::get('/presentaciones/detalle/{pres_id}',['uses'=> 'PresentacionesController@detalle'])->name('presentaciones.detalle');

        Route::get('/calificaciones',['uses'=> 'CalificacionesController@index'])->name('calificaciones');

        Route::post('calificaciones/datatable/{concurso}',['uses'=> 'CalificacionesController@datatable'])->name('calificaciones.datatable');

        Route::get('/grand-prix',['uses'=> 'CalificacionesController@grandPrix'])->name('grandprix');

        Route::get('calificar/{pres_id}',['uses'=> 'CalificacionesController@calificar'])->name('calificar');

        Route::post('calificar/guardar',['uses'=> 'CalificacionesController@saveCalificacion'])->name('calificar.guardar');

        Route::get('test/{pres_id}',['uses'=> 'CalificacionesController@test'])->name('test');
        Route::get('test-modal/{pres_id}',['uses'=> 'CalificacionesController@testmodal'])->name('test.modal');

        Route::get('/resultados',['uses'=> 'CalificacionesController@resultados'])->name('resultados');

        Route::get('/resultados/grand-prix',['uses'=> 'CalificacionesController@resultadosGrandPrix'])->name('resultados.grandprix');

        Route::post('resultados/datatable/{concurso}',['uses'=> 'CalificacionesController@datatableResultados'])->name('resultados.datatable');

        Route::get('resultados/ganadores/{concurso}',['uses'=> 'CalificacionesController@ganadores'])->name('resultados.ganadores');

        Route::get('resultado/detalle/{pres_id}',['uses'=> 'CalificacionesController@detalleCalificacion'])->name('calificacion.detalle');

        Route::get('/importar-concurso',['uses'=> 'PresentacionesController@importarConcurso'])->name('importar-concurso');

        Route::view('/importar-concurso', 'presentaciones.importar-concurso')->name('importar-concurso'); //vista grand prix

        Route::view('/importar-danza', 'presentaciones.importar-danza')->name('importar-danza');//solos pura danza

        Route::view('/importar-grupos', 'presentaciones.importar-grupos')->name('importar-grupos');//solos pura danza

   
        Route::post('/importar-grandprix',['uses'=> 'PresentacionesController@saveExcelGrandPrix'])->name('importar-grandprix');

        Route::post('/import-danza', 'PresentacionesController@saveExcelDanza')->name('import-danza');

        Route::post('/import-grupos', 'PresentacionesController@saveExcelGrupos')->name('import-grupos');

        Route::get('/btcnow',['uses'=> 'PaymentsController@ShowPriceBtcNow'])->name('payments.btc');


        Route::resource('/balance', 'BalanceController', ['parameters' => [
            'balance' => 'id'
        ]]);


        //USERS

        Route::get('users', 'UserController@index')->name('users.index');

        Route::get('users/create', 'UserController@create')->name('users.create');

        Route::post('users/store', 'UserController@store')->name('users.store');

        Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');

        Route::post('users/{user}', 'UserController@update')->name('users.update');

        Route::get('users/{user}/destroy', 'UserController@destroy')->name('users.destroy');

    //ACADEMIES

        Route::get('academies', 'AcademyController@index')->name('academies.index');

        Route::get('academies/create', 'AcademyController@create')->name('academies.create');

        Route::post('academies/store', 'AcademyController@store')->name('academies.store');

        Route::get('academies/{academy}/edit', 'AcademyController@edit')->name('academies.edit');

        Route::post('academies/{academy}', 'AcademyController@update')->name('academies.update');

        Route::get('academies/{academy}/destroy', 'AcademyController@destroy')->name('academies.destroy');

        Route::get('academies/{academy}', 'AcademyController@show')->name('academies.show');

       // Route::view('/welcome', 'welcome')->name('academies.show');


       Route::get('test-push', function () {

            $guzzle = new \GuzzleHttp\Client();

            $body=json_encode([
                "to" => "ExponentPushToken[ohMjqIBLe0kYBcMQ95eNkw]",
                "title" =>  "",
                "body"=> "Hello world!",
                "sound"=> "default"
            ]);

            $response = $guzzle->post('https://exp.host/--/api/v2/push/send', [
                'headers' => [
                    'Accept' => 'application/json',
                    'User-Agent' => 'testing/1.0',
                    'content-type' => 'application/json',
                    'Accept-Encoding'=> 'gzip, deflate'
                ],
                'body'=>$body,
            ]);

            //return $status = $response->getStatusCode();
            dd($response);
            return json_decode((string) $response->getBody(), true)['data'];

       });


        Route::get('/migrate', function () {
            try {

                echo '<br>init with tables migrations...';
              //  Artisan::call('migrate', array('--force' => true)); // here it stops via browser
                Artisan::call('migrate:refresh', array('--seed' => true)); // here it stops via browser
                echo 'done with migrations';
        
                echo '<br>clear view cache...';
                $cachedViewsDirectory = app('path.storage').'/framework/views/';
        
                if ($handle = opendir($cachedViewsDirectory)) {
                    while (false !== ($entry = readdir($handle))) {
                        if(strstr($entry, '.')) continue;
        
                        @unlink($cachedViewsDirectory . $entry);
                    }
        
                    closedir($handle);
                }
                echo 'all view cache cleared';
        
                return redirect()->route('presentaciones');
            } catch (Exception $e) {
                Response::make($e->getMessage(), 500);
            }
        });
       
       
       Route::get('print',['uses'=> 'CalificacionesController@print_result'])->name('print');
  
});


